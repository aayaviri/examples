var selectedViewNode;
var graph;
var paper;
var linking = false;
$(document).ready(function() {
	$("#company-area-id").select2({
		width : "100%"
	});
	
	graph = new joint.dia.Graph;
	paper = new joint.dia.Paper({
		el : $('#paper'),
		width : 2000,
		height : 500,
		gridSize : 1,
		model : graph
	});
	console.log(paper);

	//EVENTS
	paper.on("cell:pointerclick", function(cellView, evt, x, y) {
		if (linking == false) {
			selectNode(cellView);
		} else {
			endLinkNode(cellView);
		}
	});
	paper.on("blank:pointerclick", function(evt, x, y) {
		if (linking == false) {
			unselectNode();
		}
	});
	
	$("#formality-definition-form").on("submit", onFormSubmit);
	// END EVENTS
	initNodeDialog();
	if(loadedNodes != undefined) {
		initLoadedValues();
	}
});

function unselectNode() {
	if (selectedViewNode != undefined) {
		selectedViewNode.unhighlight();
		selectedViewNode = undefined;
	}
}

function selectNode(node) {
	unselectNode();
	selectedViewNode = node;
	selectedViewNode.highlight();
}

function initNodeDialog() {
	$("#btn-submit-node-dialog").on("click", function() {
		var description = [$("#step-name").val(), $("#step-description").val()];
		var nodeId = $("#node-id").val();
		var title = $("#company-area-"+$("#company-area-id").val()).attr("data-model-code");
		var companyAreaId = $("#company-area-id").val();
		if(nodeId == "") {
			addNode("", title, description, companyAreaId, 50, 50, false);
		} else {
			editNode(nodeId, title, description, companyAreaId);
		}
		
		$("#node-dialog").dialog("close");
	});
	
	$("#btn-cancel-node-dialog").on("click", function() {
		$("#node-dialog").dialog("close");
	});
}

function initLoadedValues() {
	for(var i=0; i<loadedNodes.length; i++) {
		addNode(loadedNodes[i].modelId, loadedNodes[i].name, loadedNodes[i].description, loadedNodes[i].companyAreaId, loadedNodes[i].x, loadedNodes[i].y, loadedNodes[i].isFirst);
	}
	
	for(var i=0; i<loadedNodes.length; i++) {
		if(loadedNodes[i].prevStepModelId != "") {
			var source = getNodeByModelId(loadedNodes[i].prevStepModelId);
			var target = getNodeByModelId(loadedNodes[i].modelId);
			link(source, target);
		}
	}
}

function showNodeDialog(isNew) {
	if (isNew == true) {
		$("#node-id").val("");
		$("#model-id").val("");
		$("#company-area-id").val("");
		$("#step-name").val("");
		$("#step-description").val("");
	} else {
		if(selectedViewNode != undefined) {
			$("#node-id").val(selectedViewNode.model.id);
			$("#company-area-id").select2("val", selectedViewNode.model.prop("companyAreaId"));
			var events = selectedViewNode.model.prop("events");
			$("#step-name").val(events[0]);
			$("#step-description").val(events[1]);
		}
		else {
			alert("Debe Seleccionar un nodo primero.");
			return;
		}
	}
	$("#node-dialog").dialog({
		width : 420,
		height : 400,
		show : "scale",
		hide : "scale",
		resizable : "false",
		// position: "center",
		modal : "true"
	});
}

function addNode(modelId, title, description, companyAreaId, x, y, isFirst) {
	var state = new joint.shapes.uml.State({
		position : {
			x : x,
			y : y
		},
		size : {
			width : 200,
			height : 200
		},
		name : title,
		events : description
	});
	state.prop("modelId", modelId);
	state.prop("companyAreaId", companyAreaId);
	state.prop("isFirst", isFirst);
	if (isFirst == true) {
		state.attr({
			rect: {stroke: "#8ECE5A", fill: "#E5EEDE"}
		});
	}
	graph.addCell(state);
}

function editNode(nodeId, title, description, companyAreaId) {
	var node = graph.getCell(nodeId);
	node.prop("name", title);
	node.prop("events", description);
	node.prop("companyAreaId", companyAreaId);
}

function deleteNode() {
	if(selectedViewNode != undefined) {
		if (selectedViewNode.model.prop("modelId") == "") {
			selectedViewNode.model.remove();
			selectedViewNode = undefined;
		} else {
			alert("El Paso no puede ser Eliminado debido a que ya fue guardado en la bsae de datos");
		}
		
	} else {
		alert("Debe seleccionar un nodo primero");
	}
}

function startLinkNode() {
	if(selectedViewNode != undefined) {
		$("#add-node-button").attr("disabled", true);
		$("#edit-node-button").attr("disabled", true);
		$("#delete-node-button").attr("disabled", true);
		$("#link-node-button").attr("disabled", true);
		linking = true;
	} else {
		alert("Debe seleccionar un nodo primero");
	}
}

function endLinkNode(targetView) {
	link(selectedViewNode.model, targetView.model);
	$("#add-node-button").attr("disabled", false);
	$("#edit-node-button").attr("disabled", false);
	$("#delete-node-button").attr("disabled", false);
	$("#link-node-button").attr("disabled", false);
	linking = false;
}

function link(source, target) {
	graph.addCell(new joint.shapes.uml.Transition({ source: { id: source.id }, target: { id: target.id }}));
}

function onFormSubmit(e) {
	var nodesData = new Array();
	var nodes = graph.getElements();
	var links = graph.getLinks();
	
	for(var i = 0; i<nodes.length; i++) {
		var events = nodes[i].prop("events");
		nodesData.push({
			"name" : "nodes["+nodes[i].id+"][id]",
			"value" : nodes[i].id
		});
		nodesData.push({
			"name" : "nodes["+nodes[i].id+"][modelId]",
			"value" : nodes[i].prop("modelId")
		});
		nodesData.push({
			"name" : "nodes["+nodes[i].id+"][companyAreaId]",
			"value" : nodes[i].prop("companyAreaId")
		});
		nodesData.push({
			"name" : "nodes["+nodes[i].id+"][name]",
			"value" : events[0]
		});
		nodesData.push({
			"name" : "nodes["+nodes[i].id+"][description]",
			"value" : events[1]
		});
		nodesData.push({
			"name" : "nodes["+nodes[i].id+"][x]",
			"value" : nodes[i].prop("position/x")
		});
		nodesData.push({
			"name" : "nodes["+nodes[i].id+"][y]",
			"value" : nodes[i].prop("position/y")
		});
		nodesData.push({
			"name" : "nodes["+nodes[i].id+"][isFirst]",
			"value" : nodes[i].prop("isFirst")
		});
	}
	
	for (var i=0; i<links.length; i++) {
		var source = links[i].prop("source");
		var target = links[i].prop("target");
		if (source.id != undefined && target.id != undefined) {
			nodesData.push({
				"name" : "nodes["+target.id+"][previousNodeId]",
				"value" : source.id
			});
		}
	}
	
    $(this).append($.map(nodesData, function (param) {
	    return   $('<input>', {
		        type: 'hidden',
		        name: param.name,
		        value: param.value
		    });
    	})
    );
}

function getNodeByModelId(modelId) {
	var nodes = graph.getElements();
	var nodeFound; 
	for(var i=0; i<nodes.length; i++) {
		if (nodes[i].prop("modelId") != "" && nodes[i].prop("modelId") == modelId) {
			nodeFound = nodes[i];
			break;
		}
	}
	return nodeFound;
}

function setFirstNode() {
	if(selectedViewNode != undefined) {
		var firstNode = getFirstNode();
		if (firstNode == null || firstNode != selectedViewNode.model) {
			if (firstNode != null) {
				firstNode.prop("isFirst", false);
				firstNode.attr({
					rect: {stroke: "#bdc3c7", fill: "#ecf0f1"}
				});
			}
			selectedViewNode.model.prop("isFirst", true);
			selectedViewNode.model.attr({
				rect: {stroke: "#8ECE5A", fill: "#E5EEDE"}
			});
		}
		unselectNode();
	} else {
		alert("Debe seleccionar un nodo primero");
	}
}

function getFirstNode() {
	var nodes = graph.getElements();
	var firstNode = null;
	$.each(nodes, function(key, value) {
		if (value.prop("isFirst") == true) {
			firstNode = value;
		}
	});
	return firstNode;
}

function test() {
	console.log(getFirstNode());
}
