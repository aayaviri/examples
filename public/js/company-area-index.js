$(document).ready(function() {
	var jsTree = $("#company-tree").jstree({
		"core" : {
			"animation" : 0,
			"check_callback" : true,
			"themes" : {
				"stripes" : true
			},
			"multiple" : false,
			'data' : {
				'dataType' : 'json',
				'url' : '/sis-tramites/public/company-area/ajax-get-children',
				'method' : "post",
				'data' : function(node) {
					var postObject = {
						"id" : node.id,
						"type" : node.type
					};
					if (node.data != undefined) {
						postObject.objectId = node.data.objectId;
					}
					return postObject;
				}
			}
		},
		"search" : {
			"case_insensitive" : false,
			"show_only_matches" : true,
			"ajax" : {
				"url" : '/sis-tramites/public/company-area/ajax-search-node',
				"dataType" : "json",
				"data" : {}
			},
		},
		"types" : {
			"#" : {
				"max_children" : 1,
				"max_depth" : 4,
				"valid_children" : ["root"]
			},
			"companyArea" : {
				"icon" : "/sis-tramites/public/images/table_sheet.png"
			}
		},
		"plugins" : ["contextmenu", "dnd", "json_data", "search", "state", "types", "wholerow"]
	});
	jsTree.on("select_node.jstree", function(e, selection) {
		loadEmployees();
	});

	//Code for the search field
	var to = false;
	$('#search-company-area').keyup(function() {
		if (to) {
			clearTimeout(to);
		}
		to = setTimeout(function() {
			var v = $('#search-company-area').val();
			jsTree.jstree("search", v);
		}, 250);
	});
	//end of the search field

	$("#company-area-form").on("submit", function(e) {
		e.preventDefault();
		var postData = $(this).serializeArray();
		$.ajax({
			"dataType" : 'json',
			"url" : $(this).prop("action"),
			"type" : "post",
			"data" : postData,
			"success" : function(data, textStatus, jqXHR) {
				if (data.success) {
					var companyTree = $("#company-tree").jstree();
					$("#company-area-dialog").dialog("close");
					companyTree.refresh_node(data.nodeParentId);
					companyTree.open_node(data.nodeParentId);
				}
			}
		});
	});

	$("#employee-id").select2();
	$("#employee-form").on("submit", function(e) {
		e.preventDefault();
		var postData = $(this).serializeArray();
		$.ajax({
			"dataType" : 'json',
			"url" : $(this).prop("action"),
			"type" : "post",
			"data" : postData,
			"success" : function(data, textStatus, jqXHR) {
				$("#employee-dialog").dialog("close");
				if (data.success == true) {
					loadEmployees();
				} else {
					alert(data.message);
				}
			}
		});
	});
});

function showCompanyAreaForm(isNew) {
	var companyTree = $("#company-tree").jstree();
	var selected = companyTree.get_selected();
	var selectedNode = companyTree.get_node(selected[0]);
	if (isNew == false) {
		$("#company-area-id").val(selectedNode.data.objectId);
		$("#company-area-code").val(selectedNode.data.objectCode);
		$("#company-area-max-employees").val(selectedNode.data.objectMaxEmployees);
		$("#company-area-name").val(selectedNode.data.objectName);
		$("#node-parent-id").val(selectedNode.parent);
	} else {
		$("#company-area-parent-id").val(selectedNode.data.objectId);
		$("#company-area-id").val("");
		$("#company-area-code").val("");
		$("#company-area-max-employees").val("1");
		$("#company-area-name").val("");
		$("#node-parent-id").val(selectedNode.id);
	}

	$("#company-area-dialog").dialog({
		width : 420,
		height : 350,
		show : "scale",
		hide : "scale",
		resizable : "false",
		// position: "center",
		modal : "true"
	});
}

function deleteCompanyArea() {
	var companyTree = $("#company-tree").jstree();
	var selected = companyTree.get_selected();
	var selectedNode = companyTree.get_node(selected[0]);
	var parentNode = selectedNode.parent;
	var postData = {
		"id" : selectedNode.data.objectId,
		"node-parent-id" : parentNode
	};
	$.ajax({
		"dataType" : 'json',
		"url" : '/sis-tramites/public/company-area/ajax-delete',
		"type" : "post",
		"data" : postData,
		"success" : function(data, textStatus, jqXHR) {
			if (data.success) {
				var companyTree = $("#company-tree").jstree();
				companyTree.refresh_node(data.nodeParentId);
				companyTree.open_node(data.nodeParentId);
			} else {
				alert(data.errorMessage);
			}
		}
	});

}

function testJsTree() {
	var companyTree = $("#company-tree").jstree();
	console.log(companyTree);
	var selected = companyTree.get_selected();
	console.log(selected);
	var selectedNode = companyTree.get_node(selected[0]);
	console.log(selectedNode);
}

function loadEmployees() {
	var companyTree = $("#company-tree").jstree();
	var selected = companyTree.get_selected();
	var selectedNode = companyTree.get_node(selected[0]);

	var postData = {
		"company-area-id" : selectedNode.data.objectId
	};
	$.ajax({
		"url" : '/sis-tramites/public/employee/ajax-get-employees',
		"type" : "post",
		"dataType" : "json",
		"data" : postData,
		"success" : function(data, textStatus, jqXHR) {
			var html = "";
			for (var i = 0; i < data.length; i++) {
				html += "<tr>";
				html += "<td>" + data[i].fullName + "</td>";
				// html += "<td>" + data[i].status + "</td>";
				html += "<td>" + data[i].username + "</td>";
				html += "<td><a href=\"#\" onclick=\"removeEmployeeFromCompanyArea(" + data[i].id + ")\">Eliminar</a></td>";
				html += "</tr>";
			}
			$("#employee-table-body").html(html);
		}
	});
}

function removeEmployeeFromCompanyArea(employeeId) {
	var postData = {
		"employee-id" : employeeId
	};
	$.ajax({
		"url" : '/sis-tramites/public/employee/ajax-remove-company-area',
		"type" : "post",
		"dataType" : "json",
		"data" : postData,
		"success" : function(data, textStatus, jqXHR) {
			loadEmployees();
		}
	});
}

function showEmployeeForm() {
	var companyTree = $("#company-tree").jstree();
	var selected = companyTree.get_selected();
	var selectedNode = companyTree.get_node(selected[0]);
	$("#employee-company-area-id").val(selectedNode.data.objectId);

	$("#employee-dialog").dialog({
		width : 420,
		height : 350,
		show : "scale",
		hide : "scale",
		resizable : "false",
		// position: "center",
		modal : "true"
	});
}