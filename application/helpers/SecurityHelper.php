<?php

class App_Helper_SecurityHelper {
	/**
	 * @var App_Model_User
	 */
	private $_sessUser;
	
	public function __construct() {
		$this->_sessUser = null;
	}
	
	public function setUser(App_Model_User $sessUser) {
		$this->_sessUser = $sessUser;
	}
	
	public function validate($rol) {
		$result = false;
		$permitedRoles = array();
		if ($this->_sessUser === null) {
			return false;
		}
		// Zend_Debug::dump($this->_sessUser->getRole());
		// Zend_Debug::dump(App_Model_User::ROL_SUPER_ADMIN);
		
		if ($this->_sessUser->getRole() === App_Model_User::ROL_USER) {
			$permitedRoles[] = App_Model_User::ROL_USER;
		} elseif ($this->_sessUser->getRole() === App_Model_User::ROL_CORRESPONDENCE_REGISTER) {
			$permitedRoles[] = App_Model_User::ROL_USER;
			$permitedRoles[] = App_Model_User::ROL_CORRESPONDENCE_REGISTER;
		} elseif ($this->_sessUser->getRole() === App_Model_User::ROL_CORRESPONDENCE_ADMIN) {
			$permitedRoles[] = App_Model_User::ROL_USER;
			$permitedRoles[] = App_Model_User::ROL_CORRESPONDENCE_REGISTER;
			$permitedRoles[] = App_Model_User::ROL_CORRESPONDENCE_MANAGER;
			$permitedRoles[] = App_Model_User::ROL_CORRESPONDENCE_ADMIN;
		} elseif ($this->_sessUser->getRole() === App_Model_User::ROL_CORRESPONDENCE_MANAGER) {
			$permitedRoles[] = App_Model_User::ROL_USER;
			$permitedRoles[] = App_Model_User::ROL_CORRESPONDENCE_REGISTER;
			$permitedRoles[] = App_Model_User::ROL_CORRESPONDENCE_MANAGER;
		} elseif ($this->_sessUser->getRole() === App_Model_User::ROL_FORMALITY_USER) {
			$permitedRoles[] = App_Model_User::ROL_USER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_USER;
		} elseif ($this->_sessUser->getRole() === App_Model_User::ROL_FORMALITY_REGISTER) {
			$permitedRoles[] = App_Model_User::ROL_USER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_USER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_REGISTER;
		} elseif ($this->_sessUser->getRole() === App_Model_User::ROL_FORMALITY_MANAGER) {
			$permitedRoles[] = App_Model_User::ROL_USER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_USER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_MANAGER;
		} elseif ($this->_sessUser->getRole() === App_Model_User::ROL_FORMALITY_ADMIN) {
			$permitedRoles[] = App_Model_User::ROL_USER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_USER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_REGISTER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_MANAGER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_ADMIN;
		} elseif($this->_sessUser->getRole() === App_Model_User::ROL_SUPER_ADMIN) {
			$permitedRoles[] = App_Model_User::ROL_USER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_USER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_REGISTER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_MANAGER;
			$permitedRoles[] = App_Model_User::ROL_FORMALITY_ADMIN;
			$permitedRoles[] = App_Model_User::ROL_CORRESPONDENCE_REGISTER;
			$permitedRoles[] = App_Model_User::ROL_CORRESPONDENCE_MANAGER;
			$permitedRoles[] = App_Model_User::ROL_CORRESPONDENCE_ADMIN;
			$permitedRoles[] = App_Model_User::ROL_SUPER_ADMIN;
		}
		
		if (in_array($rol, $permitedRoles)) {
			$result = true;
		}
		return $result;
	}
}
