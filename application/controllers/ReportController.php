<?php

class ReportController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
	}

	// List of all users
	public function indexAction() {
		//$this->view->formality = App_Model_Formality::
		// $this->view->correspondeces = App_Model_Correspondence::getAllSent(10);
		// $this->view->documentTypes = App_Model_DocumentType::getAll(1000, 0);
	}

	// Show details data of an specific user
	public function viewAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');

		$userDao = new App_Dao_UserDao();
		$this->view->user = $userDao->getById($id);
	}
	// Formalities begins this month
	public function formalitiesAction() {
		
		$formData = $this->_request->getPost();
		$formData=$this->changeDateFormat($formData);
		$from = new DateTime($formData["from"]);
		$to = new DateTime($formData["to"]);
		$this->view->formalitiesDefinition=App_Model_FormalityDefinition::getAll(100, 0);
		$this->view->stepsStarted=App_Model_Step::getAllStarted($from->format("Y-m-d h:i:s"),$to->format("Y-m-d h:i:s"));
		$this->view->stepsFinished=App_Model_Step::getAllFinished($from->format("Y-m-d h:i:s"),$to->format("Y-m-d h:i:s"));
		$this->view->stepsPending=App_Model_Step::getAllPending();
		$this->view->stepsStartedAndFinished=App_Model_Step::getAllStartedAndFinished($from->format("Y-m-d h:i:s"),$to->format("Y-m-d h:i:s"));
		$this->view->from = $from;
		$this->view->to = $to;
		
	}
	public function correspondencesAction() {
		$formData = $this->_request->getPost();
		$formData=$this->changeDateFormat($formData);
		$dateFrom = new DateTime($formData["from"]);
		$dateTo = new DateTime($formData["to"]);
		
		$this->view->sents=$this->correspondenceDirection('sent',$dateFrom->format("Y-m-d h:i:s"),$dateTo->format("Y-m-d h:i:s"));
		$this->view->receiveds=$this->correspondenceDirection('received',$dateFrom->format("Y-m-d h:i:s"),$dateTo->format("Y-m-d h:i:s"));
		$this->view->from = $dateFrom;
		$this->view->to = $dateTo;
		
	}
	public function correspondenceDirection($status,$dateFrom,$dateTo){
		
		$correspondenceHistories= App_Model_CorrespondenceHistory::getAllByStatusAndDateRange($status, $dateFrom, $dateTo);
		$areasCompany=$this->areasToArray();
		$documentTypes=$this->documentsTypeToArray();
		$areas=array();
		$documents=array();
		$correspondencesByArea=array();
		$lastAreaId=1;
		
		for ($i=0;$i< count($areasCompany);$i++){
			
			$correspondencesByArea=array();
			foreach ($correspondenceHistories as $correspondenceHistory){
					$owner=$correspondenceHistory->getCorrespondence()->getOwner();
					 
					if ($owner instanceof App_Model_Employee){
						/**
						 * @var App_Model_Employee
						 */
						$owner;
						if ($owner->getCompanyArea() != null && $areasCompany[$i]['areaId']==$owner->getCompanyArea()->getId()){
								$correspondencesByArea[]=array('documentTypeId'=>$correspondenceHistory->getCorrespondence()->getDocumentType()->getId(),'area'=>$areasCompany[$i]['areaName']);	
						}

					}
			}
										
			$documents=array();
			
			for($j=0;$j<count($documentTypes);$j++){
				foreach ($correspondencesByArea as $correspondenceByArea){					
					if($documentTypes[$j]['documentTypeId']==$correspondenceByArea['documentTypeId']){
						
						$documents[]=array('documentType'=>$documentTypes[$j]['documentTypeName']);
					}									
				}
			}
			$documentsAndTotal=array();
			$totalDocumentType=1;
			for ($k=0; $k < count($documents); $k++) {
				if (isset($documents[$k+1]['documentType']) && $documents[$k+1]['documentType']==$documents[$k]['documentType']){
					$totalDocumentType++;
				}
				else{
					$documentsAndTotal[]=array('documentType'=>$documents[$k]['documentType'],'total'=>$totalDocumentType);
					$totalDocumentType=1;
				}
			}
			if(count($documents)>0)
				array_push($areas,array('areaName'=>$areasCompany[$i]['areaName'],'correspondences'=>$documentsAndTotal));
		}
	
		return $areas;
	}	
	public function changeDateFormat($formData){
		if (isset($formData["from"]) and $formData["from"]!=""){
			list($dia, $mes, $anio) = explode("/",$formData["from"]);
			$formData["from"]=$anio."-".$mes."-".$dia." 00:00:00";
		} else {
			$formData["from"]=date("Y-m-d")." 00:00:00";
		}
		if (isset($formData["to"]) and $formData["to"]!="") {
			list($dia, $mes, $anio) = explode("/",$formData["to"]);
			$formData["to"]=$anio."-".$mes."-".$dia." 23:59:59";
		} else {
			$formData["to"]=date("Y-m-d")." 23:59:59";
		}
		return $formData;
	}
	public function areasToArray(){
		$areasCompany=App_Model_CompanyArea::getAll(1000, 0);
		$areasArray=array();
		/**
		 * @var App_Model_CompanyArea
		 */
		 $area;
		foreach ($areasCompany as $area) {
			
			$areasArray[]=array('areaId'=>$area->getId(),'areaName'=> $area->getName());
		}
		return $areasArray;
	}
	
	public function documentsTypeToArray() {
		$documentTypes=App_Model_DocumentType::getAll(1000, 0);
		$documentTypesArray=array();
		/**
		 * @var App_Model_DocumentType
		 */
		 $documentType;
		foreach ($documentTypes as $documentType) {
			
			$documentTypesArray[]=array('documentTypeId'=>$documentType->getId(),'documentTypeName'=> $documentType->getName());
		}
		return $documentTypesArray;
	}
	
	public function formalitiesDetailAction() {
		$formData = $this->_request->getPost();
		$formData=$this->changeDateFormat($formData);
		
		$from = new DateTime($formData["from"]);
		$to = new DateTime($formData["to"]);
		$this->view->from = $from;
		$this->view->to = $to;
		
		$this->view->formalityDefinitionList=App_Model_FormalityDefinition::getAll(100, 0);
		$this->view->stepsStarted=array();
		$this->view->stepsFinished= array();
		$this->view->stepsPending= array();
		$this->view->stepsStartedAndFinished= array();
		
		if (count($this->_request->getPost()) > 0) {
			
			$formalityDefinitionId = $formData["formalityDefinitionId"];
			$this->view->formalityDefinition = App_Model_FormalityDefinition::getById($formalityDefinitionId);
			$this->view->stepsStarted=App_Model_Step::getAllStartedByFormalityDefinitionId($from->format("Y-m-d h:i:s"),$to->format("Y-m-d h:i:s"), $formalityDefinitionId);
			$this->view->stepsFinished=App_Model_Step::getAllFinishedByFormalityDefinitionId($from->format("Y-m-d h:i:s"),$to->format("Y-m-d h:i:s"), $formalityDefinitionId);
			$this->view->stepsPending=App_Model_Step::getAllPendingByFormalityDefinitionId($formalityDefinitionId);
			$this->view->stepsStartedAndFinished=App_Model_Step::getAllStartedAndFinishedByFormalityDefinitionId($from->format("Y-m-d h:i:s"),$to->format("Y-m-d h:i:s"), $formalityDefinitionId);
			
		}
		
	}
}
