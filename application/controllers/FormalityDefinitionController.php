<?php

class FormalityDefinitionController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->view->activeMenuItem = "formality";
    }

    public function indexAction() {
		$this->view->formalityDefinitionList = App_Model_FormalityDefinition::getAll(10000, 0);
    }

    public function addAction() {
    	$this->view->companyAreaList = App_Model_CompanyArea::getAll(1000, 0);
	}
	
	public function viewAction() {
		$id = $this->_getParam('id', '');
		
		if (empty($id)) {
			$this->_helper->redirector('index');
		}
		
		$formalityDefinitionDao = new App_Dao_FormalityDefinitionDao();
		$this->view->formalityDefinition = $formalityDefinitionDao->getById($id);
	}
	
	public function modifyAction() {
		$id = $this->_getParam('id', '');
		if (empty($id)) {
			$this->_helper->redirector('index');
		} 
		$this->view->companyAreaList = App_Model_CompanyArea::getAll(1000, 0);
		/**
		 * @var App_Model_FormalityDefinition
		 */
		$formalityDefinition = App_Model_FormalityDefinition::getById($id); 
		
		$steps = $formalityDefinition->getSteps();
		
		$this->view->formalityDefinition = $formalityDefinition;
		$firstStep = $formalityDefinition->getFirstStep();
		
		$nodes = array();
		foreach($steps as $step) {
			/**
			 * @var App_Model_StepDefinition
			 */
			$step;
			
			$prevStepModelId = "";
			if ($step->getPrevStep() != null) {
				$prevStepModelId = $step->getPrevStep()->getId();
			}
			if($firstStep !== null && $firstStep->getId() == $step->getId()) {
				$isFirst = true;
			} else {
				$isFirst = false;
			}
			$nodes[] = array(
				"name" => $step->getResponsible()->getCode(),
				"prevStepModelId" => $prevStepModelId,
				"x" => $step->getX(),
				"y" => $step->getY(),
				"companyAreaId" => $step->getResponsible()->getId(),
				"modelId" => $step->getId(),
				"description" => array($step->getTitle(), $step->getDescription()),
				"isFirst" => $isFirst
			);
		}
		$this->view->nodes = $nodes;
	}
	
	public function saveAction() {
		$postData = $this->_request->getPost();
		
		if (array_key_exists("formality-definition-id", $postData) && $postData["formality-definition-id"] != "") {
			$formalityDefinition = App_Model_FormalityDefinition::getById($postData["formality-definition-id"]);
		} else {
			$formalityDefinition = new App_Model_FormalityDefinition($postData["code"], $postData["name"], "");
		}
		$stepDefinitions = array();
		foreach($postData["nodes"] as $node) {
			$responsible = App_Model_CompanyArea::getById(intval($node["companyAreaId"]));
			if (array_key_exists("modelId", $node) && $node["modelId"] != "") {
				/**
				 * @var App_Model_StepDefinition
				 */
				$stepDefinition = App_Model_StepDefinition::getById($node["modelId"]);
				$stepDefinition->setResponsible($responsible);
				$stepDefinition->setTitle($node["name"]);
				$stepDefinition->setDescription($node["description"]);
				$stepDefinition->setX($node["x"]);
				$stepDefinition->setY($node["y"]);
				$stepDefinition->setPrevStep(null);
			} else {
				$stepDefinition = new App_Model_StepDefinition($responsible,$node["name"], $node["description"], $node["x"], $node["y"]);
			}
			$stepDefinitions[$node["id"]] = $stepDefinition;
			
		}
		foreach($postData["nodes"] as $node) {
			
			if(array_key_exists("previousNodeId", $node) && $node["previousNodeId"] != "") {
				$stepDefinitions[$node["id"]]->setPrevStep($stepDefinitions[$node["previousNodeId"]]);
			}
			$stepDefinitions[$node["id"]]->save();
			
			$formalityDefinition->addStep($stepDefinitions[$node["id"]]);
			Zend_Debug::dump($node);
			if($node["isFirst"] == "true") {
				$formalityDefinition->setFirstStep($stepDefinitions[$node["id"]]);
			}
		}
		$formalityDefinition->save($formalityDefinition);
		$this->_helper->redirector('index');
	} 
	
	public function ajaxSetFirstStep() {
		$this->_helper->layout->disableLayout();
		$formalityDefinitionId = $this->_getParam('id', "");
		$stepDefinitionId = $this->_getParam('stepId', "");
		
		$formalityDefinition = App_Model_FormalityDefinition::getById($formalityDefinitionId);
		$stepDefinition = App_Model_StepDefinition::getById($stepDefinitionId);
		$success = true;
		$errorMessage = "";
		$formalityDefinition->setFirstStep($stepDefinition);
		$formalityDefinition->save();
		
		$arrayResponse = array("success"=>$success, "errorMessage"=> $errorMessage);
		echo json_encode($arrayResponse);
		exit;
	}
	
	public function testAction() {
		Zend_Debug::dump($this->_request->getPost());
	}
}
