<?php

class CompanyAreaController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
		$this -> view -> activeMenuItem = "settings";
	}

	public function indexAction() {
		/* Index action controller */
		$this -> view -> employeeList = App_Model_Employee::getAll(200, 0);
	}

	public function ajaxGetChildrenAction() {
		$this -> _helper -> layout -> disableLayout();

		$id = $this -> _getParam('id');
		$type = $this -> _getParam('type');
		$objectId = $this -> _getParam('objectId');

		$arrayResponse = array();
		if ($id === "#") {
			$rootArea = App_Model_CompanyArea::getRoot();
			if ($rootArea !== null) {
				$arrayChild = array();
				$arrayChild["id"] = "companyArea-" . $rootArea -> getId();
				$arrayChild["text"] = $rootArea -> getName();
				$arrayChild["type"] = "companyArea";
				$arrayChild["data"]["objectId"] = $rootArea -> getId();
				$arrayChild["data"]["objectName"] = $rootArea -> getName();
				$arrayChild["data"]["objectCode"] = $rootArea -> getCode();
				$arrayChild["data"]["objectMaxEmployees"] = $rootArea -> getMaxEmployees();
				$arrayChild["children"] = true;
				$arrayResponse[] = $arrayChild;
			}
		} elseif ($type === "companyArea") {
			$parent = App_Model_CompanyArea::getById($objectId);
			foreach ($parent->getChildren() as $childArea) {
				/**
				 * @var App_Model_CompanyArea
				 */
				$childArea;
				$arrayChild = array();
				$arrayChild["id"] = "companyArea-" . $childArea -> getId();
				$arrayChild["text"] = $childArea -> getName();
				$arrayChild["type"] = "companyArea";
				$arrayChild["data"]["objectId"] = $childArea -> getId();
				$arrayChild["data"]["objectName"] = $childArea -> getName();
				$arrayChild["data"]["objectCode"] = $childArea -> getCode();
				$arrayChild["data"]["objectMaxEmployees"] = $childArea -> getMaxEmployees();
				$arrayChild["children"] = true;
				$arrayResponse[] = $arrayChild;
			}
		}
		echo json_encode($arrayResponse);
		exit ;
	}

	public function ajaxSaveAction() {
		$this -> _helper -> layout -> disableLayout();
		$areaId = $this -> _getParam('id', "");
		$parentId = $this -> _getParam('parent-id', "");
		if ($areaId !== "") {
			$this -> _helper -> layout -> disableLayout();
			$companyArea = App_Model_CompanyArea::getById($areaId);
			$companyArea -> changeCode($this -> _getParam("code"));
			$companyArea -> changeMaxEmployees($this -> _getParam("max-employees"));
			$companyArea -> changeName($this -> _getParam("name"));
			$companyArea -> save();
		} else {
			$parent = App_Model_CompanyArea::getById($parentId);
			$newCompanyArea = new App_Model_CompanyArea($this -> _getParam('code'), $this -> _getParam('name'), $this -> _getParam('max-employees'));
			$parent -> addChild($newCompanyArea);
			$parent -> save();
		}
		$arrayResponse = array("success" => true, "id" => $areaId, "parentId" => $parentId, "nodeParentId" => $this -> _getParam("node-parent-id"));
		echo json_encode($arrayResponse);
		exit ;
	}

	public function ajaxDeleteAction() {
		$this -> _helper -> layout -> disableLayout();
		$areaId = $this -> _getParam('id', "");
		$companyArea = App_Model_CompanyArea::getById($areaId);
		$success = true;
		$errorMessage = "";
		$stepDefinitionList = App_Model_StepDefinition::getByCompanyAreaId($companyArea -> getId());
		if (count($companyArea -> getEmployees()) > 0) {
			$success = false;
			$errorMessage = "No es posible eliminar el Area seleccionada. \nElimine a los empleados asignados.";
		} elseif (count($companyArea -> getChildren()) > 0) {
			$success = false;
			$errorMessage = "No es posible eliminar el Area seleccionada. \nElimine todas las areas dependientes.";
		} elseif (count($stepDefinitionList) > 0) {
			$success = false;
			$errorMessage = "No es posible eliminar el Area seleccionada. \nDebe desvincular los siguientes pasos de Definicion de tramites:";
			foreach ($stepDefinitionList as $stepDefinition) {
				/**
				 * @var App_Model_StepDefinition
				 */
				$stepDefinition;
				$errorMessage .= "\n- " . $stepDefinition -> getFormalityDefinition() -> getName() . ": " . $stepDefinition -> getTitle();
			}
		} else {
			try {
				$companyArea -> remove();
			} catch(Exception $e) {
				$success = false;
				$errorMessage = $e -> getMessage();
			}
		}

		$arrayResponse = array("success" => $success, "errorMessage" => $errorMessage, "nodeParentId" => $this -> _getParam("node-parent-id"));
		echo json_encode($arrayResponse);
		exit ;
	}

	public function ajaxSearchNodeAction() {
		$this -> _helper -> layout -> disableLayout();
		$text = $this -> _getParam("str");
		$companyAreaFoundList = App_Model_CompanyArea::searchByNameAndCode($text);
		$nodeIdsArray = array();
		foreach ($companyAreaFoundList as $companyArea) {
			/**
			 * @var App_Model_CompanyArea
			 */
			$companyArea;
			$tmpNodeIdsArray = array();
			while ($companyArea -> getParent() != null) {
				$companyArea = $companyArea -> getParent();
				if (!in_array("companyArea-" . $companyArea -> getId(), $nodeIdsArray)) {
					$tmpNodeIdsArray[] = "companyArea-" . $companyArea -> getId();
				}
			}
			$tmpNodeIdsArray = array_reverse($tmpNodeIdsArray);
			$nodeIdsArray = array_merge($nodeIdsArray, $tmpNodeIdsArray);
		}
		header('Content-type: text/javascript');
		echo json_encode($nodeIdsArray);
		exit ;
	}

}
