<?php

class DocumentTypeController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
		$this->view->activeMenuItem = "settings";
	}

	public function indexAction() {
		$this->view->documents = App_Model_DocumentType::getAll(50, 0);
	}

	public function viewAction() {
		$id = $this->_getParam('id', '');
		if (empty($id)) {
			$this->helper->redirector('index');
		}
		$this->view->document = App_Model_DocumentType::getById($id);
	}

	public function addAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$document = new App_Model_DocumentType();
			$document->setName($formData['name']);
			$document->setCode($formData['code']);
			$document->setDescription($formData['description']);
			$document->setType($formData['type']);
			$document->save();
			$this->_redirect($formData["referer"]);
		}
	}

	public function modifyAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$documentType = App_Model_DocumentType::getById($formData["id"]);
			
			$documentType->setName($formData['name']);
			$documentType->setCode($formData['code']);
			$documentType->setDescription($formData['description']);
			$documentType->setType($formData['type']);
			$documentType->save();
			
			$this->_redirect($formData["referer"]);
		} else {
			$id = $this->_getParam('id', '');
			if (empty($id))
				$this->_redirect($_SERVER["HTTP_REFERER"]);
			$this->view->documentType = App_Model_DocumentType::getById($id);
		}
	}

	public function removeAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_redirect($_SERVER["HTTP_REFERER"]);

		$document = App_Model_DocumentType::getById($id);
		$document->remove();
		$this->_redirect($_SERVER["HTTP_REFERER"]);
	}

}
