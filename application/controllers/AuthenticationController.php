<?php

class AuthenticationController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function loginAction()
    {
        $form = new App_Form_AuthneticationForm();
		
		if($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			
			if($form->isValid($_POST)) {
				$authAdapter = new Zend_Auth_Adapter_DbTable(
					Zend_Db_Table::getDefaultAdapter(),
					"user",
					'username',
					'password'
				);
				
				$authAdapter->setIdentity($formData['_username']);
				$authAdapter->setCredential($formData['_password']);
				//en la liena de arriba aumentar la codificacion md5 (help)
				$auth = Zend_Auth::getInstance();
				$result = $auth->authenticate($authAdapter);
				
				if($result->isValid()) {
					$userDao = new App_Dao_UserDao();
					//$user = $userDao->getByUsernamePassword($formData['_username'], 
						//									$formData['_password']);
					
					$session = new Zend_Session_Namespace('systemSession');
					$session->user = $formData['_username'];
					$session->role = "administrator";
					
					$this->helper->redirector('index', 'admin');
					return;
				}
				else {
					$this->view->message = 'El usuario o contrasena son incorrectos.';
				}
				
			}
		}
		$this->view->form = $form;
    }


}



