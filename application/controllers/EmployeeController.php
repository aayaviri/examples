<?php

class EmployeeController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
		$this->view->activeMenuItem = "settings";
	}

	public function indexAction() {
		$this->view->employeeList = App_Model_Employee::getAll(1000, 0);
	}

	public function addAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$employee = new App_Model_Employee($formData['first-name'], $formData['last-name']);
			if(!empty($formData["user-id"])) {
				$employee->setUser(App_Model_User::getById($formData["user-id"]));
			}
			$employee->save();
			$this->_helper->redirector("index");
		} else {
			$this->view->userList = App_Model_User::getAll();
		}
	}

	public function viewAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_redirect($_SERVER["HTTP_REFERER"]);

		$this->view->employee = App_Model_Employee::getById($id);
	}

	public function modifyAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$employee = App_Model_Employee::getById($formData['id']);

			$employee->setName($formData['first-name']);
			$employee->setLastName($formData['last-name']);
			if(!empty($formData["user-id"])) {
				$employee->setUser(App_Model_User::getById($formData["user-id"]));
			} else {
				$employee->setUser(null);
			}
			
			$employee->save();
			$this->_helper->redirector("index");
		} else {
			$id = $this->_getParam('id', '');
			if (empty($id)) {
				$this->_redirect($_SERVER['HTTP_REFERER']);
			}
			$this->view->employee = App_Model_Employee::getById($id);
			$this->view->userList = App_Model_User::getAll();
		}
	}

	public function removeAction() {
		$id = $this->_getParam('id', '');

		if (empty($id)) {
			$this->_redirect($_SERVER["HTTP_REFERER"]);
		}

		$employee = App_Model_Employee::getById($id);
		$employee->remove();
		$this->_redirect($_SERVER["HTTP_REFERER"]);
	}
	
	public function ajaxGetEmployeesAction() {
		$this->_helper->layout->disableLayout();
		$areaId = $this->_getParam('company-area-id', "");
		$companyArea = App_Model_CompanyArea::getById($areaId);
		$success = true;
		$arrayResponse = array();
		foreach($companyArea->getEmployees() as $employee) {
			/**
			 * @var App_Model_Employee
			 */
			$employee;
			$employeeArray = array();
			$employeeArray["id"] = $employee->getId();
			$employeeArray["fullName"] = $employee->getFullName();
			$employeeArray["status"] = $employee->getStatus();
			$employeeArray["username"] = $employee->getUser()->getUsername();
			$arrayResponse[] = $employeeArray;
		}
		echo json_encode($arrayResponse);
		exit;
	}
	
	public function ajaxAsignCompanyAreaAction() {
		$this->_helper->layout->disableLayout();
		/**
		 * @var App_Model_Employee
		 */
		$employee = App_Model_Employee::getById($this->_getParam("employee-id"));
		$companyArea = App_Model_CompanyArea::getById($this->_getParam("company-area-id"));
		$success = true;
		$message = "";
		if (count($companyArea->getEmployees()) < $companyArea->getMaxEmployees()) {
			$employee->setCompanyArea($companyArea);
			$employee->save();
		} else {
			$success = false;
			$message = "No se puede registrar mas empleados al area seleccionada.";
		}
		
		$response = array("success" => $success, "message" => $message);
		echo json_encode($response);
		exit;
		// $employee->
	}
	
	public function ajaxRemoveCompanyAreaAction() {
		$this->_helper->layout->disableLayout();
		/**
		 * @var App_Model_Employee
		 */
		$employee = App_Model_Employee::getById($this->_getParam("employee-id"));
		$employee->setCompanyArea(null);
		$employee->save();
		
		echo true;
		exit;
	}
}
