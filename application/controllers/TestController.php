<?php
// session_start();
class TestController extends Zend_Controller_Action {

	public function test1Action() {
		$test = new App_Model_Test('holo');
		$_SESSION['test'] = serialize($test);
		$this->_helper->redirector('test2');
	}
	
	public function test2Action() {
		$test1 = unserialize($_SESSION['test']);
		var_dump($test1);
		if ($this->_request->getPost()) {
			var_dump('holoooo');
			var_dump($test1);
			exit;
		}
		$test1->setT2("holo2");
		$_SESSION["test"] = serialize($test1);
	}
	
	public function test3Action() {
		$test1 = unserialize($_SESSION);
	}
	
	public function testFormalityAction() {
		$formalityDefinition = new App_Model_FormalityDefinition("TRA1", "Tramite de Catastro", "Se realiza el tramite para catastro");
		$ventanillaUnica = new App_Model_CompanyArea("VENTU", "Ventanilla Unica");
		$catastro = new App_Model_CompanyArea("Catas", "Catastro");
		$legal = new App_Model_CompanyArea("Legal", "Legal");
		$oficialMayor = new App_Model_CompanyArea("OfiMayor", "Oficial Mayor");
		$ham = new App_Model_CompanyArea("HAM", "Honorable Alcalde Municipal");
		
		$step1 = new App_Model_StepDefinition($ventanillaUnica,"Requisitos: Carnet de Identida, Plano del domicilio", 10, 10);
		$formalityDefinition->addStep($step1);
		
		$step2 = new App_Model_StepDefinition($catastro, "Procesar la informacion", 20, 20);
		$formalityDefinition->addStep($step2);
		$step2->setPrevStep($step1);
		
		$step3 = new App_Model_StepDefinition($legal, "Revisar los aspectos legales", 30, 30);
		$formalityDefinition->addStep($step3);
		
		
		$step4 = new App_Model_StepDefinition($oficialMayor, "Aprobar solicitud", 40, 40);
		$formalityDefinition->addStep($step4);
		
		$step3->setPrevStep($step2);
		$step4->setPrevStep($step3);
		
		Zend_Debug::dump($formalityDefinition);
		exit;
	}
}
