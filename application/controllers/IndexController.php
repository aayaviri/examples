<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	$this->view->activeMenuItem = "index";
        // action body
    }
    
    public function dashboardAction() {
    	if(!Zend_Auth::getInstance()->hasIdentity()) {
        	$this->_redirect("user/login");
        }
    }

}

