<?php

class ActionController extends Zend_Controller_Action {
	
	public function init() {
		$this->view->activeMenuItem = "settings";
	}
	
	public function indexAction() {
		$this->view->actions = App_Model_Action::getAll(1000, 0);
	}

	public function viewAction() {
		$id = $this->_getParam('id', '');
		if (empty($id)) {
			$this->helper->redirector('index');
		}
		$action = new App_Dao_ActionDao();
		$this->view->action = $action->getById($id);
	}

	public function addAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$action = new App_Model_Action();
			$action->setcode($formData['code']);
			$action->setName($formData['name']);
			$action->save($action);

			$this->_helper->redirector('index');
		}
	}

	public function editAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$action = App_Model_Action::getById(intval($formData['id']));
			$action->setName($formData['name']);
			$action->setCode($formData['code']);
			$action->save();
			$this->_helper->redirector('index');
		} else {
			$id = $this->_getParam('id', '');
			if (empty($id)) {
				$this->_helper->redirector('index');
			}
			$this->view->action = App_Model_Action::getById($id);
		}
	}

	public function deleteAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');

		$action = App_Model_Action::getById($id);
		$action->remove();
		$this->_helper->redirector('index');
	}

}
