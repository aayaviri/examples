<?php

class UserController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
		$this->view->activeMenuItem = "settings";
	}

	// List of all users
	public function indexAction() {
		$this->view->dataList = App_Model_User::getAll();
	}

	// Show details data of an specific user
	public function viewAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');

		$userDao = new App_Dao_UserDao();
		$this->view->user = $userDao->getById($id);
	}

	// Add a new user
	public function addAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$user = new App_Model_User($formData["username"], $formData["password"], $formData["role"]);
			$user->save();

			$this->_redirect($formData["referer"]);
		}
	}

	// Modify an existing user
	public function modifyAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			/**
			 * @var App_Model_User
			 */
			$user = App_Model_User::getById($formData["id"]);
			$user->setUsername($formData['username']);
			if(!empty($formData["password"])) {
				$user->resetPassword($formData["password"]);
			}
			$user->setRole($formData['role']);
			$user->save();
			
			$this->_redirect($formData["referer"]);
		} else {
			$id = $this->_getParam('id', '');
			if (empty($id))
				$this->_redirect($_SERVER["HTTP_REFERER"]);
			$this->view->user = App_Model_User::getById($id);
		}
	}

	// Remove an existing user
	public function removeAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');

		$userDao = new App_Dao_UserDao();

		$user = $userDao->getById($id);
		if (!empty($user))
			$userDao->remove($user);

		$this->_helper->redirector('index');
		return;
	}
	
	public function loginAction() {
		if (Zend_Auth::getInstance()->hasIdentity()) {
			$this->_redirect();
		}
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$authAdapter = new App_Util_DoctrineAuthAdapter($formData["username"], $formData["password"]);
			
			$auth = Zend_Auth::getInstance();
			$result = $auth->authenticate($authAdapter);
			if ($result->isValid()) {
				$this->_redirect();
			} else {
				$errorMessages = $result->getMessages(); 
				$this->view->errorMessage = $errorMessages[0];
			}
		}
	}
	
	public function logoutAction() {
		if (Zend_Auth::getInstance()->hasIdentity()) {
			$auth = Zend_Auth::getInstance();
			$auth->clearIdentity();
		}
		$this->_helper->redirector("login");
	}

}
