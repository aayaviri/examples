<?php

class FormalityController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
		$this->view->activeMenuItem = "formality";
	}

	public function indexAction() {
		$auth = Zend_Auth::getInstance();
		/**
		 * @var App_Model_User
		 */
		$sessUser = App_Model_User::getById($auth->getIdentity());

		$employee = $sessUser->getEmployee();
		if ($employee == null) {
			$this->_helper->FlashMessenger->addMessage("");
			$this->_helper->FlashMessenger->addMessage("No puedes ver tus tramites debido a que tu usuario no esta asignado a un empleado del municipio. Contacte al administrador del sistema.");
			$this->_helper->redirector("index", 'index', null, array());
		}
		$this->view->formalityList = App_Model_Formality::getAllCurrentByEmployeeId($employee->getId());
		$serverMessages = $this->_helper->FlashMessenger->getMessages();
		if(count($serverMessages)>0) {
			if ($serverMessages[0] != "") {
				$this->view->successMessage = $serverMessages[0];
			}
			
			if ($serverMessages[1] != "") {
				$this->view->warningMessage = $serverMessages[1];
			}
			
		}
	}

	public function detailAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');

		$this->view->formality = App_Model_Formality::getById($id);
	}

	public function addAction() {
		$auth = Zend_Auth::getInstance();
		/**
		 * @var App_Model_User
		 */
		$sessUser = App_Model_User::getById($auth->getIdentity());
		$employee = $sessUser->getEmployee();
		if ($employee == null) {
			$this->_helper->FlashMessenger->addMessage("");
			$this->_helper->FlashMessenger->addMessage("No puedes registrar tramites debido a que tu usuario no esta asignado a un empleado del municipio. Contacte al administrador del sistema.");
			$this->_helper->redirector("index");
		}
		$companyArea = $employee->getCompanyArea();
		if ($companyArea == null) {
			$this->_helper->FlashMessenger->addMessage("");
			$this->_helper->FlashMessenger->addMessage("No puedes registrar tramites debido a que no estas asignado a un area del municipio. Contacte al administrador del sistema.");
			$this->_helper->redirector("index");
		}

		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$formalityDefinition = App_Model_FormalityDefinition::getById($formData["formality-definition-id"]);
			$formality = new App_Model_Formality($formalityDefinition, $formData["citizen"]);
			$formality->start($employee);
			$this->_helper->redirector("index");
		}

		$this->view->formalityDefinitionList = App_Model_FormalityDefinition::getAllStartableByCompanyArea($companyArea->getId());
	}

	public function addCommentAction() {

	}

	public function cancelAction() {

	}

	public function inprogressAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$formality = App_Model_Formality::getById($formData["formality-id"]);
			$formality->changeInProgress($formData["message"]);
			$this->_helper->redirector("detail", 'formality', null, array("id" => $formality->getId()));
		}
	}
	
	public function openAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$formality = App_Model_Formality::getById($formData["formality-id"]);
			$formality->changeOpen($formData["message"]);
			$this->_helper->redirector("detail", 'formality', null, array("id" => $formality->getId()));
		}
	}
	public function doneAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$formality = App_Model_Formality::getById($formData["formality-id"]);
			if ($formData["next-step-definition-id"] != "" && $formData["next-employee-id"] != "") {
				$nextStepDefinition = App_Model_StepDefinition::getById($formData["next-step-definition-id"]);
				$nextEmployee = App_Model_Employee::getById($formData["next-employee-id"]);
				$formality->finishStep($formData["message"], $nextStepDefinition, $nextEmployee);
			} else {
				$formality->finishStep($formData["message"]);
			}
			$this->_helper->redirector("index");
		}
	}
	
	public function returnAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$formality = App_Model_Formality::getById($formData["formality-id"]);
			
			$formality->returnStep($formData["message"]);
			$this->_helper->redirector("index");
		}
	}
	
	public function serveStatusAction() {
		$this->_helper->layout->disableLayout();
		$id = $this->_getParam("id", "");
		$formality = App_Model_Formality::getById($id);
		if ($formality != null) {
			$responseData = array();
			$responseData["estado"] = "success";
			$responseData["mensaje"] = "";
			$responseData["tramite"] = array();
			$responseData["tramite"]["id"] = $formality->getId();
			$responseData["tramite"]["codigo"] = $formality->getCode();
			$responseData["tramite"]["solicitante"] = $formality->getCitizen();
			$responseData["tramite"]["nombre"] = $formality->getFormalityDefinition()->getName();
			$responseData["tramite"]["descripcion"] = $formality->getFormalityDefinition()->getDescription();
			$responseData["tramite"]["estadoTramite"] = $formality->getLastStep()->getStatus();
			$responseData["tramite"]["areaResponsable"] = $formality->getLastStep()->getResponsible()->getCompanyArea()->getName();
			$responseData["tramite"]["funcionarioResponsable"] = $formality->getLastStep()->getResponsible()->getFullName();
			$responseData["tramite"]["fechaRecepcionEnArea"] = $formality->getLastStep()->getDateReceived();
		} else {
			$responseData = array();
			$responseData["estado"] = "error";
			$responseData["mensaje"] = "No se encontro el tramite solicitado";
		}
		echo "<pre>";
		echo json_encode($responseData);
		exit;
	}

	public function serveAction() {
		$this->_helper->layout->disableLayout();
		$id = $this->_getParam("id", "");
		$formality = App_Model_Formality::getById($id);
		if ($formality != null) {
			$responseData = array();
			$responseData["estado"] = "success";
			$responseData["mensaje"] = "";
			$responseData["tramite"] = array();
			$responseData["tramite"]["id"] = $formality->getId();
			$responseData["tramite"]["codigo"] = $formality->getCode();
			$responseData["tramite"]["solicitante"] = $formality->getCitizen();
			$responseData["tramite"]["nombre"] = $formality->getFormalityDefinition()->getName();
			$responseData["tramite"]["descripcion"] = $formality->getFormalityDefinition()->getDescription();
			$responseData["tramite"]["estadoTramite"] = $formality->getLastStep()->getStatus();
			$responseData["tramite"]["areaResponsable"] = $formality->getLastStep()->getResponsible()->getCompanyArea()->getName();
			$responseData["tramite"]["funcionarioResponsable"] = $formality->getLastStep()->getResponsible()->getFullName();
			$responseData["tramite"]["fechaRecepcionEnArea"] = $formality->getLastStep()->getDateReceived();
		} else {
			$responseData = array();
			$responseData["estado"] = "error";
			$responseData["mensaje"] = "No se encontro el tramite solicitado";
		}
		echo "<pre>";
		echo json_encode($responseData);
		exit;
	}

	public function adminAction() {
		$this->view->formalityList = App_Model_Formality::getAll();
	}
	
	public function historyAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');

		$this->view->formality = App_Model_Formality::getById($id);
	}
	
	public function oldFormalitiesAction() {
		$auth = Zend_Auth::getInstance();
		/**
		 * @var App_Model_User
		 */
		$sessUser = App_Model_User::getById($auth->getIdentity());
		$employee = $sessUser->getEmployee();
		
		if ($employee == null) {
			$this->_helper->FlashMessenger->addMessage("");
			$this->_helper->FlashMessenger->addMessage("No puedes ver tus tramites debido a que tu usuario no esta asignado a un empleado del municipio. Contacte al administrador del sistema.");
			$this->_helper->redirector("index", 'index', null, array());
		}
		$this->view->formalityList = App_Model_Formality::getAllByEmployeeId($employee->getId());
	}

}
