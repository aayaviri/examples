<?php

class ExternalPersonController extends Zend_Controller_Action {

	public function init() {
		/* Initialize action controller here */
		$this->view->activeMenuItem = "settings";
	}

	public function indexAction() {
		$this->view->externalPersons = App_Model_ExternalPerson::getAll(1000, 0);
	}

	public function viewAction() {
		$id = $this->_getParam('id', '');
		if (empty($id)) {
			$this->helper->redirector('index');
		}
		$externalPerson = new App_Dao_ExternalPersonDao();
		$this->view->externalPerson = $externalPerson->getById($id);
	}

	public function addAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();

			$externalPerson = new App_Model_ExternalPerson($formData['name'], $formData['last-name']);
			if ($formData['company'] != null && $formData['company'] != '') {
				$externalPerson->setCompanyName($formData['company']);
			}
			$externalPerson->save();

			$this->_redirect($formData['referer']);
		}
	}

	public function modifyAction() {
		if ($this->_request->getPost()) {
			$formData = $this->_request->getPost();
			$externalPerson = App_Model_ExternalPerson::getById($formData["id"]);
			$externalPerson->setName($formData['name']);
			$externalPerson->setLastName($formData['last-name']);
			if ($formData['company'] != null && $formData['company'] != '') {
				$externalPerson->setCompanyName($formData['company']);
			}
			$externalPerson->save();

			$this->_redirect($formData['referer']);
		} else {
			$id = $this->_getParam('id', '');
			if (empty($id))
				$this->_redirect($_SERVER['HTTP_REFERER']);
			$this->view->externalPerson = App_Model_ExternalPerson::getById($id);
		}
	}

	public function removeAction() {
		$id = $this->_getParam('id', '');
		if (empty($id))
			$this->_helper->redirector('index');

		$externalPerson = App_Model_ExternalPerson::getById($id);
		$externalPerson->remove();

		$this->_helper->redirector('index');
	}

}
