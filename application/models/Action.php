<?php
/**
 * Action
 *
 * @Entity
 * @Table(name="action")
 *
 */
class App_Model_Action
{
	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var string
	 *
	 * @Column(name="code", type="string", length=100, nullable=false)
	 */
	private $_code;
	/**
	 * @var string
	 *
	 * @Column(name="name", type="string", length=100, nullable=false)
	 */
	private $_name;
	
	public function getId() {
		return $this->_id;
	}
	
	public function getCode() {
		return $this->_code;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function setCode($code) {
		$this->_code = $code;
	}
	
	public function setName($name) {
		$this->_name = $name;
	}
	
	public function __toString() {
	  	$string = "Action: {";
		$string = $string. "<br />id: ".$this->_id;
		$string = $string. "<br />code: ".$this->_code;
		$string = $string. "<br />name: ".$this->_name;
		$string = $string. "<br />}";
		return $string;
	}
	
	public function save() {
		$dao = new App_Dao_ActionDao();
		$dao->save($this);
	}
	
	public function remove() {
		$dao = new App_Dao_ActionDao();
		$dao->remove($this);
	}
	
	public static function getAll($limit, $offset) {
		$dao = new App_Dao_ActionDao();
		return $dao->getAll($limit, $offset);
	}
	
	public static function countAll() {
		$dao = new App_Dao_ActionDao();
		return $dao->countAll();
	}
	
	public static function getById($id) {
		$dao = new App_Dao_ActionDao();
		return $dao->getById($id);
	}

}
