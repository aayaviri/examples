<?php
/**
 * DocumentType
 *
 * @Entity
 * @Table(name="document_type")
 *
 */
class App_Model_DocumentType {

	const EXTERNAL = "external";
	const INTERNAL = "internal";

	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var string
	 *
	 * @Column(name="name", type="string", length=100, nullable=false)
	 */
	private $_name;
	/**
	 * @var string
	 *
	 * @Column(name="code", type="string", length=100, nullable=false)
	 */
	private $_code;
	/**
	 * @var string
	 *
	 * @Column(name="description", type="string", length=100, nullable=false)
	 */
	protected $_description;
	/**
	 * @var string
	 *
	 * @Column(name="type", type="string", length=100, nullable=true)
	 */
	private $_type;
	/**
	 * @var integer
	 *
	 * @Column(name="actual_number", type="integer", nullable=false)
	 */
	private $_actualNumber = 1;

	public function getActualNumber() {
		return $this->_actualNumber;
	}

	public function incrementActualNumber() {
		$this->_actualNumber += 1;
		$this->save();
	}

	public function getId() {
		return $this->_id;
	}

	public function getName() {
		return $this->_name;
	}

	public function getCode() {
		return $this->_code;
	}

	public function getDescription() {
		return $this->_description;
	}

	public function getType() {
		return $this->_type;
	}

	public function setName($name) {
		$this->_name = $name;
	}

	public function setCode($code) {
		$this->_code = $code;
	}

	public function setDescription($description) {
		$this->_description = $description;
	}

	public function setType($type) {
		$this->_type = $type;
	}

	public function save() {
		$dao = new App_Dao_DocumentTypeDao();
		$dao->save($this);
	}

	public function remove() {
		$dao = new App_Dao_DocumentTypeDao();
		$dao->remove($this);
	}

	public static function getAll($limit, $offset) {
		$dao = new App_Dao_DocumentTypeDao();
		return $dao->getAll($limit, $offset);
	}

	public static function getAllExternal($limit, $offset) {
		$dao = new App_Dao_DocumentTypeDao();
		return $dao->getAllExternal($limit, $offset);
	}

	public static function getAllInternal($limit, $offset) {
		$dao = new App_Dao_DocumentTypeDao();
		return $dao->getAllInternal($limit, $offset);
	}

	/**
	 * @return App_Model_DocumentType
	 */
	public static function getById($id) {
		$dao = new App_Dao_DocumentTypeDao();
		return $dao->getById($id);
	}

	public function __toString() {
		$string = "Document: {";
		$string = $string . "<br />id: " . $this->_id;
		$string = $string . "<br />name: " . $this->_name;
		$string = $string . "<br />code: " . $this->_code;
		$string = $string . "<br />description: " . $this->_description;
		$string = $string . "<br />type: " . $this->_type;
		$string = $string . "<br />actualNumber: " . $this->_actualNumber;
		$string = $string . "<br />}";
		return $string;
	}

}
