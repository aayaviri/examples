<?php
/**
 * step
 *
 * @Entity
 * @Table(name="step")
 *
 */
class App_Model_Step {
	const STATUS_OPEN = "open";
	const STATUS_IN_PROGRESS = "in_progress";
	const STATUS_DONE = "done";
	const STATUS_REOPEN = "reopen";
	const STATUS_CANCELED = "canceled";
	const STATUS_FINISHED = "finished";
	const STATUS_STARTED = "started";

	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var App_Model_StepDefinition
	 *
	 * @ManyToOne(targetEntity="App_Model_StepDefinition")
	 * @JoinColumn(name="step_definicion_id", referencedColumnName="id", nullable=true)
	 */
	private $_stepDefinition;
	/**
	 * @var string
	 *
	 * @Column(name="status", type="string", length=100, nullable=true)
	 */
	private $_status;
	/**
	 * @var string
	 *
	 * @Column(name="description", type="string", length=100, nullable=true)
	 */
	private $_description;
	/**
	 * @var App_Model_Employee
	 *
	 * @ManyToOne(targetEntity="App_Model_Employee")
	 * @JoinColumn(name="responsible_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_responsible;

	/**
	 * @var App_Model_Formality
	 *
	 * @ManyToOne(targetEntity="App_Model_Formality", cascade={"persist"})
	 * @JoinColumn(name="formality_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_formality;
	/**
	 * @var datetime
	 *
	 * @Column(name="date_log", type="datetime", nullable=true)
	 */
	private $_dateLog;
	
	/**
	 * @var datetime
	 *
	 * @Column(name="date_received", type="datetime", nullable=true)
	 */
	private $_dateReceived;

	public function __construct(App_Model_StepDefinition $stepDefinition, $status, App_Model_Employee $responsible, $description, DateTime $dateReceived) {
		$this->_stepDefinition = $stepDefinition;
		$this->_status = $status;
		$this->_dateLog = new DateTime();
		$this->_responsible = $responsible;
		$this->_description = $description;
		$this->_dateReceived = $dateReceived;
	}

	public function getId() {
		return $this->_id;
	}

	public function getStepDefinition() {
		return $this->_stepDefinition;
	}

	public function getStatus() {
		return $this->_status;
	}

	public function getDescription() {
		return $this->_description;
	}
	
	public function getResponsible() {
		return $this->_responsible;
	}
	
	public function getFormality() {
		return $this->_formality;
	}
	
	public function getDateLog() {
		return $this->_dateLog;
	}
	
	public function getDateReceived() {
		return $this->_dateReceived;
	}

	public function setFormality(App_Model_Formality $formality) {
		$this->_formality = $formality;
	}
	
	public static function getById($id) {
		$dao = new App_Dao_StepDao();
		return $dao->getById($id);
	}
	
	public function save() {
		$dao = new App_Dao_StepDao();
		$dao->save($this);
	}
	
	public static function getAllAssignedToEmployee($employeeId) {
		$dao = new App_Dao_StepDao();
		return $dao->getAllAssignedToEmployee($employeeId);
	}
	public static function getAllStarted($from,$to) {
		$dao = new App_Dao_StepDao();
		return $dao->getAllStarted($from,$to);
	}
	
	public static function getAllStartedByFormalityDefinitionId($from, $to, $formalityDefinitionId) {
		$dao = new App_Dao_StepDao();
		return $dao->getAllStartedByFormalityDefinitionId($from, $to, $formalityDefinitionId);
	}
	
	public static function getAllFinished($from,$to) {
		$dao = new App_Dao_StepDao();
		return $dao->getAllFinished($from,$to);
	}
	
	public static function getAllFinishedByFormalityDefinitionId($from, $to, $formalityDefinitionId) {
		$dao = new App_Dao_StepDao();
		return $dao->getAllFinishedByFormalityDefinitionId($from, $to, $formalityDefinitionId);
	}
	
	public static function getAllPending() {
		$dao = new App_Dao_StepDao();
		return $dao->getAllPending();
	}
	
	public static function getAllPendingByFormalityDefinitionId($formalityDefinitionId) {
		$dao = new App_Dao_StepDao();
		return $dao->getAllPendingByFormalityDefinitionId($formalityDefinitionId);
	}
	
	public static function getAllStartedAndFinished($from,$to) {
		$dao = new App_Dao_StepDao();
		return $dao->getAllStartedAndFinished($from,$to);
	}
	
	public static function getAllStartedAndFinishedByFormalityDefinitionId($from,$to, $formalityDefinitionId) {
		$dao = new App_Dao_StepDao();
		return $dao->getAllStartedAndFinishedByFormalityDefinitionId($from, $to, $formalityDefinitionId);
	}
	
	public function getNaturalStatus() {
		$naturalStatus = "";
		switch ($this->_status) {
			case App_Model_Step::STATUS_OPEN:
				$naturalStatus = "Abierto";
				break;
			case App_Model_Step::STATUS_REOPEN:
				$naturalStatus = "Reabierto";
				break;
			case App_Model_Step::STATUS_IN_PROGRESS:
				$naturalStatus = "En progreso";
				break;
			case App_Model_Step::STATUS_DONE:
				$naturalStatus = "Realizado";
				break;
			case App_Model_Step::STATUS_CANCELED:
				$naturalStatus = "Cancelado";
				break;
			case App_Model_Step::STATUS_FINISHED:
				$naturalStatus = "Finalizado";
				break;
			case App_Model_Step::STATUS_STARTED:
				$naturalStatus = "Iniciado";
				break;
		}
		return $naturalStatus;
	}
}
