<?php
/**
 * User
 *
 * @Entity
 * @Table(name="user")
 *
 */
class App_Model_User {
	
	const ROL_SUPER_ADMIN = 'super_admin';
	const ROL_FORMALITY_ADMIN = 'formality_admin';
	const ROL_FORMALITY_MANAGER = 'formality_manager';
	const ROL_FORMALITY_REGISTER = 'formality_register';
	const ROL_CORRESPONDENCE_ADMIN = 'correspondence_admin';
	const ROL_CORRESPONDENCE_MANAGER = 'correspondence_manager';
	const ROL_CORRESPONDENCE_REGISTER = 'correspondence_register';
	const ROL_FORMALITY_USER = 'formality_user';
	const ROL_USER = 'user';
	
	const STATUS_ACTIVE = 'active';
	const STATUS_INACTIVE = 'inactive';
	
	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var string
	 *
	 * @Column(name="username", type="string", length=100, nullable=false)
	 */
	private $_username;
	/**
	 * @var string
	 *
	 * @Column(name="password", type="string", length=250, nullable=false)
	 */
	private $_password;

	/**
	 * @var string
	 *
	 * @Column(name="role", type="string", length=50, nullable=false)
	 */
	private $_role;
	
	/**
	 * @var string
	 *
	 * @Column(name="status", type="string", length=50, nullable=false)
	 */
	private $_status;
	
	public function __construct($username, $password, $role) {
		$this->_username = $username;
		$this->_password = md5($password);
		$this->_role = $role;
		$this->_status = static::STATUS_ACTIVE;
	}

	public function getId() {
		return $this->_id;
	}

	public function getUsername() {
		return $this->_username;
	}

	public function setUsername($username) {
		$this->_username = $username;
	}

	public function getPassword() {
		return $this->_password;
	}

	public function changePassword($oldPassword, $newPassword) {
		if (md5($oldPassword) === $this->_password){
			$this->_password = md5($newPassword);
		} else {
			throw new Exception('El password anterior no es igual al introducido.');
		}
	}
	
	public function resetPassword($newPassword) {
		$this->_password = md5($newPassword);
	}

	public function getRole() {
		return $this->_role;
	}

	public function setRole($role) {
		$this->_role = $role;
	}
	
	public function activate() {
		$this->_status = static::STATUS_ACTIVE;
	}
	
	public function inactivate() {
		$this->_status = static::STATUS_INACTIVE;
	}
	
	public function getStatus() {
		return $this->_status;
	}
	
	public function __toString() {
		$string = "User: {";
		$string = $string. "<br />id: ".$this->_id;
		$string = $string. "<br />username: ".$this->_username;
		$string = $string. "<br />password: ".$this->_password;
		$string = $string. "<br />role: ".$this->_role;
		$string = $string. "<br />}";
		return $string;
	}
	
	public function save() {
		$dao = new App_Dao_UserDao();
		$dao->save($this);
	}
	
	public function remove() {
		$dao = new App_Dao_UserDao();
		$dao->remove($this);
	}
	
	public static function login($username, $password) {
		$dao = new App_Dao_UserDao();
		return $dao->getByUsernamePassword($username, md5($password));
	}
	
	/**
	 * @return App_Model_Employee
	 */
	public function getEmployee() {
		return App_Model_Employee::getByUserId($this->_id);
	}
	
	public static function getAll() {
		$dao = new App_Dao_UserDao();
		return $dao->getAll();
	}
	
	public static function getById($id) {
		$dao = new App_Dao_UserDao();
		return $dao->getById($id);
	}
	
	public function getNaturalRol() {
		$naturalRole = "";
		switch($this->_role) {
			case App_Model_User::ROL_USER:
				$naturalRole = "Usuario Comun";
				break;
			case App_Model_User::ROL_CORRESPONDENCE_ADMIN:
				$naturalRole = "Administrador de Correspondencia";
				break;
			case App_Model_User::ROL_CORRESPONDENCE_MANAGER:
				$naturalRole = "Manager de Correspondencia";
				break;
			case App_Model_User::ROL_CORRESPONDENCE_REGISTER:
				$naturalRole = "Registrador de Correspondencia externa";
				break;
			case App_Model_User::ROL_FORMALITY_ADMIN:
				$naturalRole = "Administrador de Tramites";
				break;
			case App_Model_User::ROL_FORMALITY_MANAGER:
				$naturalRole = "Manager de Tramites";
				break;
			case App_Model_User::ROL_FORMALITY_REGISTER:
				$naturalRole = "Iniciador de Tramites";
				break;
			case App_Model_User::ROL_FORMALITY_USER:
				$naturalRole = "Usuario de Tramites";
				break;
			case App_Model_User::ROL_SUPER_ADMIN:
				$naturalRole = "Super Administrador";
				break;
		}
		return $naturalRole;
	}
}
