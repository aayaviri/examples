<?php
/**
 * Test
 *
 * @Entity
 * @Table(name="formality")
 *
 */
class App_Model_Formality {
	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var App_Model_FormalityDefinition
	 * 
	 * @ManyToOne(targetEntity="App_Model_FormalityDefinition")
	 * @JoinColumn(name="formality_definicion_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_formalityDefinition;
	/**
	 * @var App_Model_Step[]
	 * @OneToMany(targetEntity="App_Model_Step", mappedBy="_formality", cascade={"all"})
	 */
	private $_stepHistory;
	/**
	 * @var string
	 *
	 * @Column(name="citizen", type="string", length=100, nullable=false)
	 */
	private $_citizen;
	/**
	 * @var string
	 *
	 * @Column(name="code", type="string", length=100, nullable=false)
	 */
	private $_code;
	/**
	 * @var App_Model_Step
	 * 
	 * @ManyToOne(targetEntity="App_Model_Step")
	 * @JoinColumn(name="last_step_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_lastStep;
	
	public function __construct(App_Model_FormalityDefinition $formalityDefinition, $citizen) {
		$this->_stepHistory = array();
		$this->_formalityDefinition = $formalityDefinition;
		$this->_citizen = $citizen;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function getFormalityDefinition() {
		return $this->_formalityDefinition;
	}
	
	public function getCitizen() {
		return $this->_citizen;
	}
	
	public function getCode() {
		return $this->_code;
	}
	
	/**
	 * @return App_Model_Step
	 */
	public function getLastStep() {
		return $this->_lastStep;
	}
	
	/**
	 * @return App_Model_Step
	 */
	public function getPreviousStep() {
		$dao = new App_Dao_StepDao();
		return $dao->getFormalityPreviousStep($this->_id);
	}
	
	public function start(App_Model_Employee $employee) {
		if ($employee->getCompanyArea() == null) {
			throw new Exception("El empleado asignado no esta asigna a ningun area");
		}
		$formalityStep = $this->_formalityDefinition->getFirstStep();
		$this->_code = $this->_formalityDefinition->getCode();
		
		if ($formalityStep === null) {
			throw new Exception("El tramite no tiene paso inicial para iniciarse");
		}
		
		$firstStep = new App_Model_Step($formalityStep, App_Model_Step::STATUS_STARTED, $employee, "Tramite Iniciado", new DateTime());
		$firstStep->setFormality($this);
		$firstStep->save();
		
		$step = new App_Model_Step($formalityStep, App_Model_Step::STATUS_OPEN, $employee, "", $firstStep->getDateReceived());
		$step->setFormality($this);
		$step->save();
		
		
		$this->_lastStep = $step;
		
		$this->save();
		$this->_code = $this->_code . $this->getId();
		$this->save();
	}
	
	public function changeReopen($message) {
		$lastStep = $this->getLastStep();
		$step = new App_Model_Step($lastStep->getStepDefinition(), App_Model_Step::STATUS_REOPEN, $lastStep->getResponsible(), $message, new DateTime());
		$step->setFormality($this);
		$step->save();
		$this->_lastStep = $step;
		$this->save();
	}
	
	public function changeOpen($message) {
		$lastStep = $this->getLastStep();
		$step = new App_Model_Step($lastStep->getStepDefinition(), App_Model_Step::STATUS_OPEN, $lastStep->getResponsible(), $message, $lastStep->getDateReceived());
		$step->setFormality($this);
		$step->save();
		$this->_lastStep = $step;
		$this->save();
	}
	
	public function finishStep($message, $nextStepDefinition = null, $employee = null) {
		$lastStep = $this->getLastStep();
		$stepDefinition = $lastStep->getStepDefinition();
		$step = new App_Model_Step($stepDefinition, App_Model_Step::STATUS_DONE, $lastStep->getResponsible(), $message, $lastStep->getDateReceived());
		$step->setFormality($this);
		$step->save();
		
		if ($nextStepDefinition != null && $employee != null) {
			$newStep = new App_Model_Step($nextStepDefinition, App_Model_Step::STATUS_OPEN, $employee, "Tramite Recibido", new DateTime());
			$newStep->setFormality($this);
			$newStep->save();
			$this->_lastStep = $newStep;
		} else {
			$finishStep = new App_Model_Step($stepDefinition, App_MOdel_Step::STATUS_FINISHED, $lastStep->getResponsible(), "Tramite Finalizado", $lastStep->getDateReceived());
			$finishStep->setFormality($this);
			$finishStep->save();
			$this->_lastStep = $finishStep;
		}
		$this->save();
	}
	
	public function returnStep($message) {
		$lastStep = $this->getLastStep();
		$prevStepDefinition = $lastStep->getStepDefinition()->getPrevStep();
		$step = new App_Model_Step($prevStepDefinition, App_Model_Step::STATUS_OPEN, $lastStep->getResponsible(), $message, new DateTime());
		$step->setFormality($this);
		$step->save();
		$this->_lastStep = $step;
		$this->save();
	}
	
	public function changeInProgress($message) {
		$lastStep = $this->getLastStep();
		$step = new App_Model_Step($lastStep->getStepDefinition(), App_Model_Step::STATUS_IN_PROGRESS, $lastStep->getResponsible(), $message, $lastStep->getDateReceived());
		$step->setFormality($this);
		$step->save();
		$this->_lastStep = $step;
		$this->save();
	}
	
	public function addComment($comment) {
		$lastStep = $this->getLastStep();
		$step = new App_Model_Step($lastStep->getStepDefinition(), $lastStep->getStatus(), $lastStep->getResponsible(), $message, $lastStep->getDateReceived(), $lastStep->getDateReceived());
		$step->setFormality($this);
		$step->save();
		$this->_lastStep = $step;
		$this->save();
	}
	
	public function changeResponsible($message, App_Model_Employee $employee) {
		$lastStep = $this->getLastStep();
		$step = new App_Model_Step($lastStep->getStepDefinition(), App_Model_Step::STATUS_IN_PROGRESS, $lastStep->getResponsible(), $message, new DateTime());
		$step->setFormality($this);
		$step->save();
		$this->_lastStep = $step;
		$this->save();
	}
	
	public function save() {
		$dao = new App_Dao_FormalityDao();
		$dao->save($this);
	}
	
	/**
	 * @return App_Model_Formality
	 */
	public static function getById($id) {
		$dao = new App_Dao_FormalityDao();
		return $dao->getById($id);
	}
	
	/**
	 * @return App_Model_Formality[]
	 */
	public static function getAllCurrentByEmployeeId($employeeId) {
		$dao = new App_Dao_FormalityDao();
		return $dao->getAllCurrentByEmployeeId($employeeId);
	}
	public static function getAll() {
		$dao = new App_Dao_FormalityDao();
		return $dao->getAll();
	}
	
	public function getStepHistory() {
		return $this->_stepHistory;
	}
	
	public static function getAllByEmployeeId($employeeId) {
		$dao = new App_Dao_FormalityDao();
		return $dao->getAllByEmployeeId($employeeId);
	}
	
}
