<?php
/**
 * AssignedAction
 *
 * @Entity
 * @Table(name="assigned_action")
 *
 */
class App_Model_AssignedAction
{
	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var App_Model_Action
	 * 
	 * @ManyToOne(targetEntity="App_Model_Action")
	 * @JoinColumn(name="action_id", referencedColumnName="id")
	 **/
	private $_action;
	/**
	 * @ManyToOne(targetEntity="App_Model_Correspondence", inversedBy="_assignedActions", fetch="EAGER")
	 * @JoinColumn(name="correspondence_id", referencedColumnName="id")
	 **/
	private $_correspondence;
	
	public function __construct($action) {
		$this->_action = $action;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * @return App_Model_Action
	 */
	public function getAction() {
		return $this->_action;
	}
	
	public function getCorrespondence() {
		return $this->_correspondence;
	}
	
	public function setCorrespondence($correspondence) {
		$this->_correspondence = $correspondence;
	}
	
	public function save() {
		$dao = new App_Dao_AssignedActionDao();
		$dao->save($this);
	}
	
	public function remove() {
		$dao = new App_Dao_AssignedActionDao();
		$dao->remove($this);
	}
	
	public function __toString() {
	  	$string = "Action: {";
		$string = $string. "<br />id: ".$this->_id;
		$string = $string. "<br />actionName: ".$this->_action;
		$string = $string. "<br />}";
		return $string;
	}

}
