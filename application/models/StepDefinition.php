<?php

/**
 * StepDefinition
 *
 * @Entity
 * @Table(name="step_defintion")
 */
class App_Model_StepDefinition {
	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var integer
	 *
	 * @Column(name="x", type="integer", nullable=true)
	 */
	private $_x;
	/**
	 * @var integer
	 *
	 * @Column(name="y", type="integer", nullable=true)
	 */
	private $_y;
	/**
	 * @var App_Model_StepDefinition 
	 *
	 * @ManyToOne(targetEntity="App_Model_StepDefinition", cascade={"persist"})
	 * @JoinColumn(name="prev_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_previousStep;
	/**
	 * @var App_Model_CompanyArea
	 *
	 * @ManyToOne(targetEntity="App_Model_CompanyArea")
	 * @JoinColumn(name="area_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_responsible;
	/**
	 * @var string
	 * 
	 * @Column(name="title", type="string", nullable=true)
	 */
	private $_title;
	
	/**
	 * @var string
	 * 
	 * @Column(name="description", type="string", nullable=true)
	 */
	private $_description;
	
	/**
	 * @OneToMany(targetEntity="App_Model_StepDefinition", mappedBy="_previousStep", cascade={"all"})
	 */
	private $_nextSteps;
	/**
	 * @var App_Model_FormalityDefinition
	 *
	 * @ManyToOne(targetEntity="App_Model_FormalityDefinition", cascade={"persist"})
	 * @JoinColumn(name="formality_definition_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_formalityDefinition;
	
	public function __construct(App_Model_CompanyArea $responsible, $title, $description, $x, $y) {
		$this->_title = $title;
		$this->_responsible = $responsible;
		$this->_previousStep = null;
		$this->_nextSteps = array();
		$this->_description = $description;
		$this->_x = $x;
		$this->_y = $y;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function getX() {
		return $this->_x;
	}
	
	public function getY() {
		return $this->_y;
	}
	
	public function getPrevStep() {
		return $this->_previousStep;
	}
	
	public function getNextSteps() {
		return $this->_nextSteps;
	}
	
	public function getResponsible() {
		return $this->_responsible;
	}
	
	public function getDescription() {
		return $this->_description;
	}
	
	public function setX($x) {
		$this->_x = $x;
	}
	
	public function setY($y) {
		$this->_y = $y;
	}
	
	public function setTitle($title) {
		$this->_title = $title;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function setFormalityDefinition(App_Model_FormalityDefinition $formalityDefinition) {
		$this->_formalityDefinition = $formalityDefinition;
	}
	
	public function getFormalityDefinition() {
		return $this->_formalityDefinition;
	}
	
	public function setPrevStep($previousStep) {
		$this->_previousStep = $previousStep;
	}
	
	public function setResponsible(App_Model_CompanyArea $responsible) {
		$this->_responsible = $responsible;
	}
	
	public function setDescription($description) {
		$this->_description = $description;
	}
	
	public static function getById($id) {
		$dao = new App_Dao_StepDefinitionDao();
		return $dao->getById($id);
	}
	
	function save() {
		$dao = new App_Dao_StepDefinitionDao();
		$dao->save($this);
	}
	
	public static function getByCompanyAreaId($companyAreaId) {
		$dao = new App_Dao_StepDefinitionDao();
		return $dao->getAllByCompanyAreaId($companyAreaId);
	}
}