<?php
/**
 * Correspondence
 *
 * @Entity
 * @Table(name="company_area")
 *
 */
class App_Model_CompanyArea {
	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var string
	 *
	 * @Column(name="name", type="string", length=100, nullable=true)
	 */
	private $_name;
	
	/**
	 * @var string
	 *
	 * @Column(name="code", type="string", length=100, nullable=true)
	 */
	private $_code;

	/**
	 * @var App_Model_CompanyArea
	 *
	 * @ManyToOne(targetEntity="App_Model_CompanyArea")
	 * @JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
	 *
	 **/
	private $_parent;

	/**
	 * @var App_Model_CompanyArea[]
	 * @OneToMany(targetEntity="App_Model_CompanyArea", mappedBy="_parent", cascade={"all"})
	 */
	private $_children;
	
	/**
	 * @var App_Model_Employee[]
	 * @OneToMany(targetEntity="App_Model_Employee", mappedBy="_companyArea", cascade={"all"})
	 */
	private $_employees;
	
	/**
	 * @var int
	 *
	 * @Column(name="max_employees", type="integer", length=100, nullable=false)
	 */
	private $_maxEmployees;
	
	public function __construct($code, $name, $maxEmployees = 1) {
		$this->_code = $code;
		$this->_name = $name;
		$this->_maxEmployees = $maxEmployees;
	}

	public function getId() {
		return $this->_id;
	}

	public function getName() {
		return $this->_name;
	}
	
	public function getCode() {
		return $this->_code;
	}
	
	public function getMaxEmployees() {
		return $this->_maxEmployees;
	}
	
	public function changeMaxEmployees($maxEmployees) {
		$this->_maxEmployees = $maxEmployees;
	}
	
	public function setParent(App_Model_CompanyArea $parent) {
		$this->_parent = $parent;
	}
	
	public function getParent() {
		return $this->_parent;
	}
	
	/**
	 * @return App_Model_CompanyArea[]
	 */
	public function getChildren() {
		return $this->_children;
	}
	
	public function addChild(App_Model_CompanyArea $childArea) {
		$childArea->setParent($this);
		$this->_children[] = $childArea;
	}
	
	public function addEmployee(App_Model_Employee $employee) {
		if (count($this->_employees) >= $this->_maxEmployees) {
			throw new Exception("No se pueden agregar mas empleados al area correspondiente");
		}
		$employee->setCompanyArea($this);
		$this->_employees[] = $employee;
	}
	
	/**
	 * @return App_Model_Employee[]
	 */
	public function getEmployees() {
		return $this->_employees;
	}

	public function changeName($newName) {
		$this->_name = $newName;
	}
	
	public function changeCode($newCode) {
		$this->_code = $newCode;
	}

	public function remove() {
		$dao = new App_Dao_CompanyAreaDao();
		if (count($this->getChildren()) > 0) {
			throw new Exception("El area no puede ser eliminada debido a que no se ecuentra vacia");
		}
		$dao->remove($this);
	}
	
	public function changeParent(App_Model_CompanyArea $newParent) {
		$this->_parent = $newParent;
	}

	/**
	 * @return App_Model_CompanyArea
	 */
	public static function getById($id) {
		$dao = new App_Dao_CompanyAreaDao();
		return $dao->getById($id);
	}

	public static function getRoot() {
		$dao = new App_Dao_CompanyAreaDao();
		return $dao->getRoot();
	}

	public function save() {
		$dao = new App_Dao_CompanyAreaDao();
		$dao->save($this);
	}
	
	public static function getAll($limit, $offset) {
		$dao = new App_Dao_CompanyAreaDao();
		return $dao->getAll($limit, $offset);
	}
	
	public static function searchByNameAndCode($text) {
		$dao = new App_Dao_CompanyAreaDao();
		return $dao->searchByNameAndCode($text);
	}
}
