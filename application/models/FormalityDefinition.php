<?php

/**
 * FormalityDefinition
 *
 * @Entity
 * @Table(name="formality_definition")
 *
 */
class App_Model_FormalityDefinition {
	
	/**
	 * @var integer
	 *
	 * @Column(name="id", type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="IDENTITY")
	 */
	private $_id;
	/**
	 * @var string
	 *
	 * @Column(name="code", type="string", length=50, nullable=false)
	 */
	private $_code;
	/**
	 * @var string
	 *
	 * @Column(name="name", type="string", length=100, nullable=false)
	 */
	private $_name;
	/**
	 * @var string
	 *
	 * @Column(name="description", type="string", length=100, nullable=false)
	 */
	private $_description;
	/**
	 * App_Model_StepDefinition[]
	 *
	 * @OneToMany(targetEntity="App_Model_StepDefinition", mappedBy="_formalityDefinition", cascade={"all"})
	 */
	private $_steps;
	
	/**
	 * @var App_Model_StepDefinition
	 * 
	 * @ManyToOne(targetEntity="App_Model_StepDefinition")
	 * @JoinColumn(name="first_step_definition_id", referencedColumnName="id")
	 */
	private $_firstStep;
	
	public function __construct($code, $name, $description) {
		$this->_code = $code;
		$this->_name = $name;
		$this->_description = $description;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function getCode() {
		return $this->_code;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function getDescription() {
		return $this->_description;
	}
	
	public function getSteps() {
		return $this->_steps;
	}
	
	public function getFirstStep() {
		return $this->_firstStep;
	}
	
	public function setCode($code) {
		$this->_code = $code;
	}
	
	public function setName($name) {
		$this->_formalityName = $name;
	}
	
	public function setDescription($description) {
		$this->_description = $description;
	}
	
	public function setFirstStep(App_Model_StepDefinition $firstStep) {
		$this->_firstStep = $firstStep;
	}
	
	public function addStep(App_Model_StepDefinition $step) {
		if ($step->getFormalityDefinition() == null || $step->getFormalityDefinition()->getId() != $this->_id) {
			$step->setFormalityDefinition($this);
			$this->_steps[] = $step;
		}
	}

	public function toArray() {
		return get_object_vars($this);
	}
	
	public function __toString() {
		$string = "FormalityDefinition: {";
		$string = $string. "<br />id: ".$this->_id;
		$string = $string. "<br />name: ".$this->_formalityName;
		$string = $string. "<br />description: ".$this->_formalityDescription;
		$string = $string. "<br /> steps: " .$this->_steps;
		$string = $string. "<br />}";
		return $string;
	}
	
	public static function getAll($limit, $offset) {
		$dao = new App_Dao_FormalityDefinitionDao();
		return $dao->getAll($limit, $offset);
	}
	
	public static function getById($id) {
		$dao = new App_Dao_FormalityDefinitionDao();
		return $dao->getById($id);
	}
	
	public function save() {
		$dao = new App_Dao_FormalityDefinitionDao();
		$dao->save($this);
	}
	
	public static function getAllStartableByCompanyArea($companyAreaId) {
		$dao = new App_Dao_FormalityDefinitionDao();
		return $dao->getAllStartableByCompanyArea($companyAreaId);
	}
}
