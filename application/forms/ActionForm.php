<?php

class App_Form_ActionForm extends Zend_Form
{
	public function __construct()
	{
		parent::__construct();
		
		$this->setMethod('post');
		
		$this->setAttrib('class', 'bootstrap-frm');
		
		$code = new Zend_Form_Element_Text('_code');
		$code->setLabel("Codigo:");
		$code->setRequired(true);
		
		$actionName = new Zend_Form_Element_Text('_name');
		$actionName->setLabel("Nombre:");
		$actionName->setRequired(true);
		
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
		
		$this->addElements(array($code, $actionName, $submit));
	}
}

