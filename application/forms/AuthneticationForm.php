<?php

class App_Form_AuthneticationForm extends Zend_Form
{
	public function __construct() {
			
		parent::__construct();
		$this->setMethod('post');
		
		$user = new Zend_Form_Element_Text('_username');
		$user->setLabel('Usuario:');
		$user->setRequired(true);
		
		$password = new Zend_Form_Element_Password('_password');
		$password->setLabel('Contrasena:');
		$password->setRequired(true);
		
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'INGRESAR'));
		
		$this->addElements(array($user, $password, $submit));
	}
}

