<?php

class App_Form_EmployeeForm extends Zend_Form
{
    public function __construct()
	{
		parent::__construct();
		
		$this->setMethod('post');
		
		$employeeName = new Zend_Form_Element_Text('_employeeName');
		$employeeName->setLabel("Nombre:");
		$employeeName->setRequired(true);
		
		$employeeLastName = new Zend_Form_Element_Text('_employeeLastName');
		$employeeLastName->setLabel("Apellido:");
		$employeeLastName->setRequired(true);
		
		$status = new Zend_Form_Element_Select('_status');
		$status->addMultiOption('active', 'Activo');
		$status->addMultiOption('inactive', 'Inactivo');
		$status->setLabel('Estado:');
		$status->setRequired(true);
		
		// Submit button
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
		
		$this->addElements(array($employeeName, $employeeLastName, $status, $submit));
	}	
}
