<?php

class App_Form_StepStatusForm extends Zend_Form
{
	public function __construct() {
		parent::__construct();
		$this->setMethod('post');

		$stepStatusName = new Zend_Form_Element_Text('_name');
		$stepStatusName->setLabel('Nombre');
		$stepStatusName->setRequired(true);
		
		$stepStatusDate = new Zend_Form_Element_Text('_date');
		$stepStatusDate->setLabel('Fecha');
		$stepStatusDate->setRequired(true);
		
		
		
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
		
		$this->addElements(array($stepStatusName, $stepStatusDate, $submit));
	}
}
