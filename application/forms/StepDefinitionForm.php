<?php

class App_Form_StepDefinitionForm extends Zend_Form
{
public function __construct()
	{
		parent::__construct();
		
		$this->setMethod('post');
		
		$x = new Zend_Form_Element_Text('_x');
		$x->setLabel('Eje X:');
		$x->setRequired(true);
		
		$description = new Zend_Form_Element_Textarea('_descriptions');
		$description->setLabel('Descripcion:');
		$description->setRequired(true);
		
		$y = new Zend_Form_Element_Text('_y');
		$y->setLabel('Eje Y:');
		$y->setRequired(true);
		

		
		//var_dump($description);
		// Submit button
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
		
		$this->addElements(array($x, $y, $description, $submit));
	}	
}

