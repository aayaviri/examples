<?php

class App_Form_CorrespondenceForm extends Zend_Form
{

	public function __construct()
	{
		parent::__construct();
		
		$this->setMethod('post');
		
		$controlCode = new Zend_Form_Element_Text('_controlCode');
		$controlCode->setLabel("Codigo:");
		$controlCode->setRequired(true);
		
		$subject = new Zend_Form_Element_Text('_subject');
		$subject->setLabel('Asunto:');
		$subject->setRequired(true);
		
		$filePath = new Zend_Form_Element_Text('_filePath');
		$filePath->setLabel('Ruta de la correspondencia:');
		
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
		
		$this->addElements(array($controlCode, $subject, $filePath, $submit));
	}
}

