<?php

class App_Form_PositionForm extends Zend_Form
{
	public function __construct()
	{
		parent::__construct();
	
		$this->setMethod('post');
	
		$username = new Zend_Form_Element_Text('_positionName');
		$username->setLabel("nombre:");
		$username->setRequired(true);

		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
	
		$this->addElements(array($username, $submit));
	}
}
