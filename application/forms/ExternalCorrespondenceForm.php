<?php

class App_Form_ExternalCorrespondenceForm extends Zend_Form {

    public function __construct($isNew = true) {
        parent::__construct();

        $this->setMethod('post');
        
        $this->setAttrib('class', 'bootstrap-frm');
		
        $subject = new Zend_Form_Element_Text('_subject');
        $subject->setLabel('Referencia:');
        $subject->setRequired(true);

        $referer = new Zend_Form_Element_Hidden('_referer');

        $cbxExternalDocuments = new Zend_Form_Element_Select('_externalDocument');
        $cbxExternalDocuments->setLabel('Tipo de Documento:');
        
        $cbxExternalDocuments->setRequired($isNew);
        
		if(!$isNew) {
			$cbxExternalDocuments->setAttrib('disabled', 'disabled');
		}
		
        $externalDocuments = App_Model_DocumentType::getAllExternal(100, 0);
        foreach ($externalDocuments as $externalDocument) {
            	$cbxExternalDocuments->addMultiOption($externalDocument->getId(), $externalDocument->getName());
        }
		
		$externalPerson = new Zend_Form_Element_Select('_externalPerson');
		$externalPerson->setLabel('Remitente:');
		$externalPerson->setRequired(TRUE);
		
		$externalPersonDao = new App_Dao_ExternalPersonDao();
		$externalPersonList = $externalPersonDao->getAll();
		foreach ($externalPersonList as $person) {
			$message = $person->getName() .' '. $person->getLastName();
			if($person->getCompanyName() != null) {
				$message .= ' ('.$person->getCompanyName().')';
			}
				
			$externalPerson->addMultiOption($person->getId(), $message);
		}
		
		$baseUrlHelper = new Zend_View_Helper_BaseUrl();
        $link = $baseUrlHelper->baseUrl('external-person/add');
  		$externalPerson->setRequired(true)
			   		   ->setDescription('<a href="'.$link .'" >Nuevo</a>')
			   		   ->setDecorators(array( 'ViewHelper',
        							  		  array('Description', array('escape' => false, 'tag' => false)),
        									  array('HtmlTag', array('tag' => 'dd')),
        									  array('Label', array('tag' => 'dt')),
        								'Errors',
     			 		));

        $submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
        $submit->setAttrib('class', 'button');
        $this->addElements(array($cbxExternalDocuments, $subject, $externalPerson, $submit, $referer));
    }
}