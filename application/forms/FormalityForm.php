<?php

class App_Form_FormalityForm extends Zend_Form
{
	public function __construct()
	{
		parent::__construct();
	
		$this->setMethod('post');
	
		$citizen = new Zend_Form_Element_Text('_citizen');
		$citizen->setLabel("Ciudadano:");
		$citizen->setRequired(true);
	
		$code = new Zend_Form_Element_Text('_code');
		$code->setLabel("Codigo :");
		$code->setRequired(true);
	
		// Submit button
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
	
		$this->addElements(array($citizen, $code, $submit));
	}
}
