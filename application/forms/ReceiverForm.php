<?php

class App_Form_ReceiverForm extends Zend_Form {

	public function __construct($endPointList) {
		parent::__construct();

        $this->setMethod('post');
        
        $this->setAttrib('class', 'bootstrap-frm');
		
        $referer = new Zend_Form_Element_Hidden('_referer');

        $cbxEndPointList = new Zend_Form_Element_Select('_endPointList');
        $cbxEndPointList->setLabel('Receptor:');
        $cbxEndPointList->setRequired(true);
        
        foreach ($endPointList as $endPoint) {
        	/**
			 * @var App_Model_EndPoint
			 */
			$endPoint;
			
            $cbxExternalDocuments->addMultiOption($endPoint->getId(), $endPoint->getName());
        }
		
        $submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
        $submit->setAttrib('class', 'button');
        $this->addElements(array($cbxEndPointList, $submit, $referer));
	}

}
