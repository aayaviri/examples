<?php

class App_Form_ExternalPersonForm extends Zend_Form
{
	public function __construct() {
		parent::__construct();
		$this->setMethod('post');
        
        $this->setAttrib('class', 'bootstrap-frm');
		
        $referer = new Zend_Form_Element_Hidden('_referer');
        
		$externalPersonName = new Zend_Form_Element_Text('_name');
		$externalPersonName->setLabel('Nombre');
		$externalPersonName->setRequired(true);
		
		$externalPersonLastName = new Zend_Form_Element_Text('_lastName');
		$externalPersonLastName->setLabel('Apellido');
		$externalPersonLastName->setRequired(true);
		
		$companyName = new Zend_Form_Element_Text('_companyName');
		$companyName->setLabel('Nombre de la Compañia');
		$companyName->setRequired(false);
		
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
		
		$this->addElements(array($referer, $externalPersonName, $externalPersonLastName, $companyName, $submit));
	}
}
