<?php

class App_Form_DocumentTypeForm extends Zend_Form
{
	public function __construct()
	{
		parent::__construct();
		
		$this->setMethod('post');
		
		$this->setAttrib('class', 'bootstrap-frm');
		
		$dname = new Zend_Form_Element_Text('_name');
		$dname->setLabel("Nombre:");
		$dname->setRequired(true);
		
		$code = new Zend_Form_Element_Text('_code');
		$code->setLabel("Codigo:");
		$code->setRequired(true);
		
		$description = new Zend_Form_Element_Textarea('description');
		$description->setLabel("Descripcion:");
		$description->setRequired(true);
		
        $type = new Zend_Form_Element_Select('_type');
		$type->setLabel('Tipo:');
		$type->setRequired(true);
		$type->setMultiOptions(array('disp'=>'- SELECT -', App_Model_DocumentType::EXTERNAL=>'Externa', App_Model_DocumentType::INTERNAL=>'Interna'));
		
		// Submit button
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
		
		$this->addElements(array($dname, $code,$type, $description, $submit));
	}	
}
