<?php

class App_Form_FormalityDefinition extends Zend_Form {
	
	public function __construct() {
		parent::__construct();
		$this->setMethod('post');
		
		$name = $this->createElement('text', '_formalityName');
		$name->setRequired(true)
			  ->setLabel('Nombre');
		
		$description = $this->createElement('textArea', '_formalityDescription');
		$description->setRequired(true)
					->setLabel('Descripcion');
		
		$steps = $this->createElement('text', '_steps');
		$steps->setRequired(true)
		->setLabel('Pasos');
		
		$this->addElement($name);
		$this->addElement($description);
		$this->addElement($steps);
		$this->addElement('submit', 'submit', array('ignore' => true, 'label' => 'Guardar'));
	}
}