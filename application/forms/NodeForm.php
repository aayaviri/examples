<?php

class App_Form_NodeForm extends Zend_Form
{
	public function __construct()
	{
		parent::__construct();
	
		$this->setMethod('post');
		
		$level = new Zend_Form_Element_Text('_level');
		$level->setLabel('Nivel:');
		$level->setRequired(true);
		
		$message = new Zend_Form_Element_Text('_message');
		$message->setLabel("Mensaje:");
		$message->setRequired(true);	
		// Submit button
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
	
		$this->addElements(array($level, $message, $submit));
	}
}

