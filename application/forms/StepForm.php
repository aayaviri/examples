<?php

class App_Form_StepForm extends Zend_Form
{
	public function __construct()
	{
		parent::__construct();
	
		$this->setMethod('post');
		
		$status = new Zend_Form_Element_Text('_status');
		$status->setLabel('Estado:');
		$status->setRequired(true);
		
		$messageRecieved = new Zend_Form_Element_Text('_messageRecieved');
		$messageRecieved->setLabel("Mensaje:");
		$messageRecieved->setRequired(true);	
		// Submit button
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
	
		$this->addElements(array($status, $messageRecieved, $submit));
	}
}

