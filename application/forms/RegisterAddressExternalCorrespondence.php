<?php

class App_Form_RegisterAddressExternalCorrespondence extends Zend_Form
{

    public function __construct() {
        parent::__construct();
		$this->setMethod('post');
		$this->setAttrib('class', 'bootstrap-frm');
		$elements = array();
		
		$owner = new Zend_Form_Element_Select('_owner');
		$owner->setLabel('Nombre:');
		$owner->setRequired(TRUE);
		$elements[] = $owner;
		$employeeDao = new App_Dao_EmployeeDao();
		foreach($employeeDao->getAll() as $employee) {
			$owner->addMultiOptions($employee->getId(), $employee->getName() ." ". $employee->getLastName());
		}
		
		$actionsDao = new App_Dao_ActionDao();
		$actionForm = new Zend_Form_Element_MultiCheckbox('_actions');
		$actionForm->setLabel('Acciones');		
		foreach($actionsDao->getAll() as $action) {
			$actionForm->addMultiOption($action->getId(), $action->getName());	
		}
		$elements[] = $actionForm;
		
		$message = new Zend_Form_Element_Textarea('_message');
		$message->setLabel('Mensaje');
		$message->setRequired(TRUE);
		$elements[] = $message;
		
		$submit = new Zend_Form_Element_Submit('submit', array('label' => 'GUARDAR'));
		$elements[] = $submit;
		
		$this->addElements($elements);
    }
}
