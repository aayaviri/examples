<?php

class App_Util_DoctrineAuthAdapter implements Zend_Auth_Adapter_Interface {
	
	private $_login;
	private $_password;
	
	public function __construct($login, $password) {
		$this->_login = $login;
		$this->_password = $password;
	}
	
    /**
     * authenticate() - defined by Zend_Auth_Adapter_Interface.  This method is called to
     * attempt an authentication.  Previous to this call, this adapter would have already
     * been configured with all necessary information to successfully connect to a database
     * table and attempt to find a record matching the provided identity.
     *
     * @throws Zend_Auth_Adapter_Exception if answering the authentication query is impossible
     * @return Zend_Auth_Result
     */
    public function authenticate()
    {
    	$user = App_Model_User::login($this->_login, $this->_password);
		$message = array();
		$identity = null;
		if ($user !== null) {
			$code = Zend_Auth_Result::SUCCESS;
			$message[] = "Usuario Autenticado Correctamente";
			$identity = $user->getId();
		} else {
			$code = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
			$message[] = "El login o la contraseña son incorrectas";
			$identity = null;
		}
    	return new Zend_Auth_Result($code,$identity, $message);
    }
}