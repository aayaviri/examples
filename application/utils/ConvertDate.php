<?php
/*
 * clase que busca en un arreglo todos los objetos DateTime
 * y les da formato regresa el mismo arreglo con el DateTime formateado
 */

class App_Util_ConvertDate
{
	private $_result = array();	
	
	public function __construct($arrayDate) {
		foreach ($arrayDate as $key => $option) {
			$this->_result[$key] = $option;	
			if($key == '_date') {
				$this->_result[$key] = $option->format('Y-m-d');
			}
		}
	}
	
	public function getResult() {
		return $this->_result;
	}
}
	