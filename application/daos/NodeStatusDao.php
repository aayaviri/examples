<?php
class App_Dao_NodeStatusDao {
	private $_entityManager;
	
	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}
	
	public function save(App_Model_NodeStatus $nodeStatus) {
		$this->_entityManager->persist($nodeStatus);
		$this->_entityManager->flush();
	}
	
	public function remove(App_Model_NodeStatus $nodeStatus) {
		$this->_entityManager->remove($nodeStatus);
		$this->_entityManager->flush();
	}
	
	public function getById($id) {
		return $this->_entityManager->find("App_Model_NodeStatus", $id);
	}
	
	public function getAll() {
		$query = $this->_entityManager->createQuery("SELECT n FROM App_Model_NodeStatus n");
		return $query->getResult();
	}
	
		public function countAll() {
		$query = $this->_entityManager->createQuery("SELECT COUNT(n) FROM App_Model_NodeStatus n");
		return $query->getSingleScalarResult();
	}
	
	public function getAllLimitOffset($limit, $offset)
	{
		$query = $this->_entityManager->createQuery('SELECT n FROM App_Model_Action n')
									  ->setFirstResult($offset)
		  							  ->setMaxResults($limit);
		return $query->getResult();
	}
}
