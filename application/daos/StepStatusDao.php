<?php
class App_Dao_StepStatusDao {
    private $_entityManager;
	
	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}
	
	public function save(App_Model_StepStatus $stepStatus) {
		$this->_entityManager->persist($stepStatus);
		$this->_entityManager->flush();
	}
	
	public function remove(App_Model_StepStatus $stepStatus) {
		$this->_entityManager->remove($stepStatus);
		$this->_entityManager->flush();
	}
	
	public function getById($id) {
		return $this->_entityManager->find("App_Model_StepStatus", $id);
	}
	
	public function getAll() {
		$query = $this->_entityManager->createQuery("SELECT e FROM App_Model_StepStatus e");	
		return $query->getResult();
	}
	
	public function countAll() {
		$query = $this->_entityManager->createQuery("SELECT COUNT(e) FROM App_Model_StepStatus e");
		return $query->getSingleScalarResult();
	}
	public function getAllLimitOffset($limit, $offset)
	{
		$query = $this->_entityManager->createQuery('SELECT e FROM App_Model_StepStatus e')
									  ->setFirstResult($offset)
		  							  ->setMaxResults($limit);
		return $query->getResult();
	}
}