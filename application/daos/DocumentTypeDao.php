<?php

class App_Dao_DocumentTypeDao {
	private $_entityManager;

	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}

	public function save(App_Model_DocumentType $documentType) {
		$this->_entityManager->persist($documentType);
		$this->_entityManager->flush();
	}

	public function remove(App_Model_DocumentType $documentType) {
		$this->_entityManager->remove($documentType);
		$this->_entityManager->flush();
	}

	public function getById($id) {
		return $this->_entityManager->find("App_Model_DocumentType", $id);
	}

	public function countAll() {
		$query = $this->_entityManager->createQuery("SELECT COUNT(e) FROM App_Model_DocumentType e");
		return $query->getSingleScalarResult();
	}

	public function getAll($limit, $offset) {
		$query = $this->_entityManager->createQuery('SELECT e FROM App_Model_DocumentType e')->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}

	public function getAllExternal($limit, $offset) {
		$query = $this->_entityManager->createQuery('SELECT d FROM App_Model_DocumentType d where d._type = \'' . App_Model_DocumentType::EXTERNAL . '\'')->setFirstResult($offset)->setMaxResults($limit);
		;

		return $query->getResult();
	}

	public function getAllInternal($limit, $offset) {
		$query = $this->_entityManager->createQuery('SELECT d FROM App_Model_DocumentType d where d._type = \'' . App_Model_DocumentType::INTERNAL . '\'')->setFirstResult($offset)->setMaxResults($limit);
		;

		return $query->getResult();
	}

}
