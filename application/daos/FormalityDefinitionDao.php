<?php

class App_Dao_FormalityDefinitionDao {
	
	private $_entityManager;
	
	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}
	
	public function save(App_Model_FormalityDefinition $formalityDefinition) {
		$this->_entityManager->persist($formalityDefinition);
		$this->_entityManager->flush();
	}
	
	public function remove(App_Model_FormalityDefinition $formalityDefinition) {
		$this->_entityManager->remove($formalityDefinition);
		$this->_entityManager->flush();
	}
	
	public function getById($id) {
		return $this->_entityManager->find("App_Model_FormalityDefinition", $id);
	}
	
	public function countAll() {
		$query = $this->_entityManager->createQuery('SELECT COUNT(f) FROM App_Model_FormalityDefinition f');
		return $query->getSingleScalarResult();	
	}
	
	public function getAll($limit, $offset) {
		$query = $this->_entityManager->createQuery('SELECT f FROM App_Model_FormalityDefinition f')
		->setFirstResult($offset)
		->setMaxResults($limit);
	
		return $query->getResult();
	}
	
	public function getAllStartableByCompanyArea($companyAreaId) {
		$sql = "select fd from App_Model_FormalityDefinition fd join fd._firstStep fs join fs._responsible as r where r._id = ". $companyAreaId;
		$query = $this->_entityManager->createQuery($sql);
		return $query->getResult();
	}
}