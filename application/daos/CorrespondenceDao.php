<?php
class App_Dao_CorrespondenceDao {
	private $_entityManager;

	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}

	public function save(App_Model_Correspondence $correspondence) {
		$this->_entityManager->persist($correspondence);
		$this->_entityManager->flush();
	}

	public function remove(App_Model_Correspondence $correspondence) {
		$this->_entityManager->remove($correspondence);
		$this->_entityManager->flush();
	}

	public function getById($id) {
		return $this->_entityManager->find("App_Model_Correspondence", $id);
	}

	public function getAllExternalToReceive($limit, $offset) {
		$sql = 'SELECT c FROM App_Model_Correspondence as c JOIN c._correspondenceBody as b JOIN b._documentType as d';
		$sql .= ' where c._status in (\'' . App_Model_Correspondence::STATUS_TO_SEND . '\', \'' . App_Model_Correspondence::STATUS_SENT . '\')';
		$sql .= ' and d._type = \'' . App_Model_DocumentType::EXTERNAL . '\'';
		$query = $this->_entityManager->createQuery($sql)->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}

	public function countAll() {
		$query = $this->_entityManager->createQuery('SELECT COUNT(n) FROM App_Model_Correspondence n');
		return $query->getSingleScalarResult();
	}

	public function getAllLimitOffset($limit, $offset) {
		$query = $this->_entityManager->createQuery('SELECT n FROM App_Model_Correspondence n')->setFirstResult($offset)->setMaxResults($limit);

		return $query->getResult();
	}

	public function getByFatherNode($originId) {
		$query = $this->_entityManager->createQuery("SELECT n FROM App_Model_Correspondence n WHERE n.previus_node_id = '" . $originId . "'");
		return $query->getResult();
	}

	public function getToSendByOwnerId($ownerId, $limit, $offset) {
		$sql = 'select c from App_Model_Correspondence c where c._ownerId = ' . $ownerId . ' and c._status = \'' . App_Model_Correspondence::STATUS_TO_SEND . '\'';
		$query = $this->_entityManager->createQuery($sql)->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}
	
	public function getSentByOwnerId($ownerId, $limit, $offset) {
		$sql = "select c from App_Model_Correspondence c join c._owner o where o._id = " . $ownerId . " and 
		c._status in ('" . App_Model_Correspondence::STATUS_SENT . "'
		, '".App_Model_Correspondence::STATUS_TO_SEND."')";
		$query = $this->_entityManager->createQuery($sql)->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}
	
	public function getArchivedByOwnerId($ownerId, $limit, $offset) {
		$sql = "select c from App_Model_Correspondence c join c._owner o where o._id = " . $ownerId . " and 
		c._status in ('".App_Model_Correspondence::STATUS_ARCHIVED."')";
		$query = $this->_entityManager->createQuery($sql)->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}
	
	public function getAllReceivedAndToReceiveByOwnerId($ownerId, $limit, $offset) {
		$sql = 'select c from App_Model_Correspondence c join c._owner o where o._id = ' . $ownerId . ' and (c._status = \'' . App_Model_Correspondence::STATUS_WAITING . '\'';
		$sql .= ' or c._status = \'' . App_Model_Correspondence::STATUS_RECEIVED . '\'';
		$sql .= ' or c._status = \'' . App_Model_Correspondence::STATUS_TO_RESEND . '\'';
		$sql .= ' or c._status = \'' . App_Model_Correspondence::STATUS_RESENT . '\')';
		$query = $this->_entityManager->createQuery($sql)->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}
	
	public function getToReceiveByOwnerId($ownerId, $limit, $offset) {
		$sql = 'select c from App_Model_Correspondence c join c._owner o where o._id = ' . $ownerId . ' and c._status = \'' . App_Model_Correspondence::STATUS_WAITING . '\'';
		$query = $this->_entityManager->createQuery($sql)->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}
	
	public function getAll($limit, $offset) {
		$sql = "select c from App_Model_Correspondence c";
		$query = $this->_entityManager->createQuery($sql)->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}
	
}
