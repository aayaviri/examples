<?php
class App_Dao_PositionDao {
	private $_entityManager;
	
	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}

	public function save(App_Model_Position $position) {
		$this->_entityManager->persist($position);
		$this->_entityManager->flush();
	}
	
	public function remove(App_Model_Position $position) {
		$this->_entityManager->remove($position);
		$this->_entityManager->flush();
	}
	
	public function getById($id) {
		return $this->_entityManager->find("App_Model_Position", $id);
	}
	
	public function getAll() {
		$query = $this->_entityManager->createQuery('SELECT u FROM App_Model_Position u');
		return $query->getResult();
	}
	
	public function countAll() {
		$query = $this->_entityManager->createQuery('SELECT COUNT(u) FROM App_Model_Position u');
		return $query->getSingleScalarResult();
	}
	
	public function getAllLimitOffset($limit, $offset)
	{
		$query = $this->_entityManager->createQuery('SELECT u FROM App_Model_Position u')
		->setFirstResult($offset)
		->setMaxResults($limit);
	
		return $query->getResult();
	}
}
