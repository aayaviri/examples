<?php
class App_Dao_CompanyAreaDao {
	private $_entityManager;

	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}

	public function save(App_Model_CompanyArea $companyArea) {
		$this->_entityManager->persist($companyArea);
		$this->_entityManager->flush();
	}

	public function remove(App_Model_CompanyArea $companyArea) {
		$this->_entityManager->remove($companyArea);
		$this->_entityManager->flush();
	}

	public function getById($id) {
		return $this->_entityManager->find("App_Model_CompanyArea", $id);
	}

	public function getRoot() {
		$dql = "select ca from App_Model_CompanyArea ca where ca._parent is null";
		$query = $this->_entityManager->createQuery($dql);
		$result = $query->getResult();
		if (count($result) > 0) {
			return $result[0];
		} else {
			return null;
		}
	}

	public function getAll($limit, $offset) {
		$query = $this->_entityManager->createQuery('SELECT n FROM App_Model_CompanyArea n')->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}
	
	public function searchByNameAndCode($text) {
		$sql = "SELECT c from App_Model_CompanyArea c where c._name like '%".$text."%' or c._code like '%".$text."%'";
		$query = $this->_entityManager->createQuery($sql)->setFirstResult(0)->setMaxResults(10);
		return $query->getResult();
	}

}
