<?php
class App_Dao_CorrespondenceHistoryDao {
	private $_entityManager;
	
	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}
	
	public function save(App_Model_CorrespondenceHistory $correspondenceHistory) {
		$this->_entityManager->persist($correspondenceHistory);
		$this->_entityManager->flush();
	}
	
	public function remove(App_Model_CorrespondenceHistory $correspondenceHistory) {
		$this->_entityManager->remove($correspondenceHistory);
		$this->_entityManager->flush();
	}
	
	public function getById($id) {
		return $this->_entityManager->find("App_Model_CorrespondenceHistory", $id);
	}
	
	public function getAll() {
		$query = $this->_entityManager->createQuery('SELECT s FROM App_Model_CorrespondenceHistory s');
		return $query->getResult();
	}
	public function getAllByStatusAndDateRange($status,$dateFrom,$dateTo){
		$query = $this->_entityManager->createQuery("SELECT s FROM App_Model_CorrespondenceHistory s where s._status='".$status."' and s._dateLog BETWEEN '".$dateFrom."' and '".$dateTo."'");
		return $query->getResult();
	}
	
	public function countAll() {
		$query = $this->_entityManager->createQuery('SELECT COUNT(s) FROM App_Model_CorrespondenceHistory s');
		return $query->getSingleScalarResult();
	}
	
}
