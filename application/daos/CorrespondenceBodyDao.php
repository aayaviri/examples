<?php
class App_Dao_CorrespondenceBodyDao {
	private $_entityManager;
	
	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}
	
	public function save(App_Model_CorrespondenceBody $correspondenceBody) {
		$this->_entityManager->persist($correspondenceBody);
		$this->_entityManager->flush();
	}
	
	public function remove(App_Model_CorrespondenceBody $correspondenceBody) {
		$this->_entityManager->remove($correspondenceBody);
		$this->_entityManager->flush();
	}
	public function getById($id) {
		return $this->_entityManager->find("App_Model_CorrespondenceBody", $id);
	}
	
	public function countAll() {
		$query = $this->_entityManager->createQuery('SELECT COUNT(c) FROM App_Model_CorrespondenceBody c');
		return $query->getSingleScalarResult();
	}
	
	public function getAll($limit, $offset)
	{
		$query = $this->_entityManager->createQuery('SELECT c FROM App_Model_CorrespondenceBody c')
									  ->setFirstResult($offset)
									  ->setMaxResults($limit);
		
		return $query->getResult();
	}
}