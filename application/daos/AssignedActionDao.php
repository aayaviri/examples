<?php
class App_Dao_AssignedActionDao {
	private $_entityManager;
	
	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}
	
	public function save(App_Model_AssignedAction $assignedAction) {
		$this->_entityManager->persist($assignedAction);
		$this->_entityManager->flush();
	}
	
	public function remove(App_Model_AssignedAction $assignedAction) {
		$this->_entityManager->remove($assignedAction);
		$this->_entityManager->flush();
	}
	
	public function getById($id) {
			return $this->_entityManager->find("App_Model_AssignedAction", $id);
	}
	
	public function countAll() {
		$query = $this->_entityManager->createQuery("SELECT COUNT(e) FROM App_Model_AssignedAction e");
		return $query->getSingleScalarResult();
	}
	
	public function getAll($limit, $offset)
	{
		$query = $this->_entityManager->createQuery('SELECT e FROM App_Model_AssignedAction e')
									  ->setFirstResult($offset)
		  							  ->setMaxResults($limit);
		return $query->getResult();
	}
}
