<?php
class App_Dao_FormalityDao {
private $_entityManager;
	
	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}

	public function save(App_Model_Formality $formality) {
		$this->_entityManager->persist($formality);
		$this->_entityManager->flush();
	}
	
	public function remove(App_Model_Formality $formality) {
		$this->_entityManager->remove($formality);
		$this->_entityManager->flush();
	}
	
	public function getById($id) {
		return $this->_entityManager->find("App_Model_Formality", $id);
	}
	
	public function getAll() {
		$query = $this->_entityManager->createQuery('SELECT f FROM App_Model_Formality f');
		return $query->getResult();
	}
	
	public function countAll() {
		$query = $this->_entityManager->createQuery('SELECT COUNT(f) FROM App_Model_Formality f');
		return $query->getSingleScalarResult();
	}
	
	public function getAllLimitOffset($limit, $offset)
	{
		$query = $this->_entityManager->createQuery('SELECT f FROM App_Model_Formality f')
		->setFirstResult($offset)
		->setMaxResults($limit);
	
		return $query->getResult();
	}
	
	public function getAllCurrentByEmployeeId($employeeId) {
		$sql = "select f from App_Model_Formality as f join f._lastStep as ls join ls._responsible as r where r._id = ". $employeeId." and ls._status in ('".App_Model_Step::STATUS_IN_PROGRESS."', '".App_Model_Step::STATUS_REOPEN."', '".App_Model_Step::STATUS_OPEN."')";
		$query = $this->_entityManager->createQuery($sql);
		return $query->getResult();
	}
	
	public function getAllByEmployeeId($employeeId) {
		$sql = "select DISTINCT f from App_Model_Formality as f join f._stepHistory as sh join sh._responsible as r where r._id = ". $employeeId;
		$query = $this->_entityManager->createQuery($sql);
		return $query->getResult();
	}
	
}
