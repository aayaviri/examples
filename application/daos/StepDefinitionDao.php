<?php
class App_Dao_StepDefinitionDao {
	private $_entityManager;

	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}
	
	public function save(App_Model_StepDefinition $stepDefinition) {
		$this->_entityManager->persist($stepDefinition);
		$this->_entityManager->flush();
	}
	
	public function remove (App_Model_StepDefinition $stepDefintion) {
		$this->_entityManager->remove($stepDefintion);
		$this->_entityManager->flush();
	}
	
	public function getById($id) {
		return $this->_entityManager->find("App_Model_StepDefinition", $id);
	}
	
	public function getAll() {
		$query = $this->_entityManager->createQuery('SELECT u FROM App_Model_StepDefinition u');
		return $query->getResult();
	}
	

	public function countAll() {
		$query = $this->_entityManager->createQuery('SELECT COUNT(u) FROM App_Model_StepDefinition u');
		return $query->getSingleScalarResult();
	}
	
	public function getAllLimitOffset($limit, $offset)
	{
		$query = $this->_entityManager->createQuery('SELECT u FROM App_Model_StepDefinition u')
								->setFirstResult($offset)
								->setMaxResults($limit);
		
		return $query->getResult();
	}
	
	public function getAllByCompanyAreaId($companyAreaId)
	{
		$dql = "select u from App_Model_StepDefinition u join u._responsible r where r._id = ". $companyAreaId;
		$query = $this->_entityManager->createQuery($dql);
		
		return $query->getResult();
	}
}
