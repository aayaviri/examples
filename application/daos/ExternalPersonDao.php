<?php
class App_Dao_ExternalPersonDao {
	private $_entityManager;

	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}

	public function save(App_Model_ExternalPerson $externalPerson) {
		$this->_entityManager->persist($externalPerson);
		$this->_entityManager->flush();
	}

	public function remove(App_Model_ExternalPerson $externalPerson) {
		$this->_entityManager->remove($externalPerson);
		$this->_entityManager->flush();
	}

	public function getById($id) {
		return $this->_entityManager->find("App_Model_ExternalPerson", $id);
	}

	public function countAll() {
		$query = $this->_entityManager->createQuery("SELECT COUNT(e) FROM App_Model_ExternalPerson e");
		return $query->getSingleScalarResult();
	}

	public function getAll($limit, $offset) {
		$query = $this->_entityManager->createQuery('SELECT e FROM App_Model_ExternalPerson e')->setFirstResult($offset)->setMaxResults($limit);
		return $query->getResult();
	}

}
