<?php
class App_Dao_StepDao {
	private $_entityManager;
	
	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this->_entityManager = $registry->entityManager;
	}
	
	public function save(App_Model_Step $step) {
		$this->_entityManager->persist($step);
		$this->_entityManager->flush();
	}
	
	public function remove(App_Model_Step $step) {
		$this->_entityManager->remove($step);
		$this->_entityManager->flush();
	}
	
	public function getById($id) {
		return $this->_entityManager->find("App_Model_Step", $id);
	}
	
	public function getAll() {
		$query = $this->_entityManager->createQuery('SELECT s FROM App_Model_Step s');
		return $query->getResult();
	}
	public function getAllStarted($from,$to) {
		$query = $this->_entityManager->createQuery("SELECT s FROM App_Model_Step s where s._status='started' AND
													s._dateLog BETWEEN '".$from."' AND '".$to."'");
		return $query->getResult();
	}
	
	public function getAllStartedByFormalityDefinitionId($from,$to, $formaliltyDefinitionId) {
		$dql = "SELECT s FROM App_Model_Step s 
				join s._formality f
				join f._formalityDefinition fd
				where s._status='started' 
					and fd._id = ".$formaliltyDefinitionId."
					AND s._dateLog BETWEEN '".$from."' AND '".$to."'";
		$query = $this->_entityManager->createQuery($dql);
		return $query->getResult();
	}
	
	public function getAllFinished($from,$to) {
		$query = $this->_entityManager->createQuery("SELECT s FROM App_Model_Step s where s._status='finished' AND
													s._dateLog BETWEEN '".$from."' AND '".$to."'");
		return $query->getResult();
	}
	public function getAllFinishedByFormalityDefinitionId($from,$to,$formalityDefinitionId) {
		$dql = "SELECT s FROM App_Model_Step s
				join s._formality f 
				join f._formalityDefinition fd
				where s._status='finished'
					and fd._id = ".$formalityDefinitionId." 
					AND s._dateLog BETWEEN '".$from."' AND '".$to."'";
		$query = $this->_entityManager->createQuery($dql);
		return $query->getResult();
	}
	
	public function getAllPending() {
		$sql = "SELECT s FROM App_Model_Step s join s._formality f 
				where s._status='started' 
				and f._id not in (
					select g._id from App_Model_Step ms join ms._formality g
					where ms._status='canceled' or ms._status='finished'
				)";
		$query = $this->_entityManager->createQuery($sql);
		return $query->getResult();
	}
	
	public function getAllPendingByFormalityDefinitionId($formalityDefinitionId) {
		$sql = "SELECT s FROM App_Model_Step s 
				join s._formality f 
				join f._formalityDefinition fd
				where s._status='started'
				and fd._id = ".$formalityDefinitionId." 
				and f._id not in (
					select g._id from App_Model_Step ms 
					join ms._formality g
					join g._formalityDefinition gd
					where (ms._status='canceled' or ms._status='finished')
					and gd._id= ".$formalityDefinitionId."
				)";
		$query = $this->_entityManager->createQuery($sql);
		return $query->getResult();
	}
	
	public function getAllStartedAndFinished($from,$to) {
		$query = $this->_entityManager->createQuery("SELECT s FROM App_Model_Step s join s._formality f where s._status='started' AND
													s._dateLog BETWEEN '".$from."' AND '".$to."' and f._id in(SELECT g._id from App_Model_Step t join t._formality g
																													where t._status='finished' AND
																													t._dateLog BETWEEN '".$from."' AND '".$to."')");
		return $query->getResult();
	}
	
	public function getAllStartedAndFinishedByFormalityDefinitionId($from,$to, $formalityDefinitionId) {
		$dql = "SELECT s FROM App_Model_Step s 
				join s._formality f 
				join f._formalityDefinition fd
				where s._status='started' 
				AND fd._id = ".$formalityDefinitionId."
				AND s._dateLog BETWEEN '".$from."' AND '".$to."' 
				AND f._id in(
					SELECT g._id from App_Model_Step t 
					join t._formality g
					join g._formalityDefinition gd
					where t._status='finished'
					AND gd._id = ".$formalityDefinitionId."
					AND t._dateLog BETWEEN '".$from."' AND '".$to."'
					)";
		$query = $this->_entityManager->createQuery($dql);
		return $query->getResult();
	}
	
	public function countAll() {
		$query = $this->_entityManager->createQuery('SELECT COUNT(s) FROM App_Model_Step s');
		return $query->getSingleScalarResult();
	}
	
	public function getAllLimitOffset($limit, $offset)
	{
		$query = $this->_entityManager->createQuery('SELECT s FROM App_Model_Step s')
									  ->setFirstResult($offset)
									  ->setMaxResults($limit);
		
		return $query->getResult();
	}
	
	public function getAllAssignedToEmployee($employeeId) {
		//TODO: SELECCIONAR SOLO LOS ULTIMOS NODOS QUE ESTOY YO DE RESPONSIBLE
		$sql = "select max(sx.id) from App_Model_Step sx join sx._formality fx where fx._id = SELECT s FROM App_Model_Step s join s._responsible r where r._id = ". $employeeId. " and s._status in ('open', 'reopen', 'in_progress') order by s._dateReceived";
		$query = $this->_entityManager->createQuery($sql);
		
		return $query->getResult();
	}
	
}
