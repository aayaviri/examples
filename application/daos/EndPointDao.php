<?php

class App_Dao_EmployeeDao {
	private $_entityManger;

	public function __construct() {
		$registry = Zend_Registry::getInstance();
		$this -> _entityManager = $registry -> entityManager;
	}

	public function save(App_Model_Employee $employee) {
		$this -> _entityManager -> persist($employee);
		$this -> _entityManager -> flush();
	}

	public function remove(App_Model_Employee $employee) {
		$this -> _entityManager -> remove($employee);
		$this -> _entityManager -> flush();
	}

	public function getById($id) {
		return $this -> _entityManager -> find("App_Model_Employee", $id);
	}

	public function getAll($limit, $offset) {
		$query = $this -> _entityManager -> createQuery('SELECT e FROM App_Model_Employee e') -> setFirstResult($offset) -> setMaxResults($limit);
		return $query -> getResult();
	}

	public function countAll() {
		$query = $this -> _entityManager -> createQuery('SELECT COUNT (e) FROM App_Model_Employee e');
		return $query -> getSingleScalarResult();
	}

	public function getByNameLastName($name, $lastName) {
		$query = $this -> _entityManager -> createQuery("SELECT e FROM App_Model_Employee e WHERE 
													e.name ='" . $name . "' and e.last_name = '" . $lastName . "'");
		$arrayResult = $query -> getResult();

		if ($arrayResult != null) {
			return $arrayResult[0];
		} else {
			return null;
		}
	}

	public function getAllLimitOffset($limit, $offset) {
		$query = $this -> _entityManager -> createQuery('SELECT e FROM App_Model_Employee e') -> setFirstResult($offset) -> setMaxResults($limit);

		return $query -> getResult();
	}

}
