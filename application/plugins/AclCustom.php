<?php
Zend_Session::start();

class AclCustomPlugin extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if( !(Zend_Registry::isRegistered('acl')) )
		{
			$acl = new Zend_Acl();

			$acl->addRole(new Zend_Acl_Role('admin'));
			
			$acl->add(new Zend_Acl_Resource('index'));
			$acl->add(new Zend_Acl_Resource('error'));
			$acl->add(new Zend_Acl_Resource('authentication'));
			$acl->add(new Zend_Acl_Resource('action'));
			$acl->add(new Zend_Acl_Resource('correspondence'));
			$acl->add(new Zend_Acl_Resource('document'));
			$acl->add(new Zend_Acl_Resource('employee'));
			$acl->add(new Zend_Acl_Resource('formality'));
			$acl->add(new Zend_Acl_Resource('node'));
			$acl->add(new Zend_Acl_Resource('position'));
			$acl->add(new Zend_Acl_Resource('step'));
			$acl->add(new Zend_Acl_Resource('user'));
			
			$acl->allow('admin');
			Zend_Registry::set('acl', $acl);
		}
		$auth = Zend_Registry::getInstance();
		
		$session = new Zend_Session_Namespace('systemSession');
		
		$loginController = 'authentication';
		$loginAction = 'action';
		
		if(!$auth->hasIdentity()) {
			$registry = Zend_Registry::getInstance();
			$acl = $registry->get('acl');
			
			$isAllowed = $acl->isAllowed($session->role,
										 $request->getControllerName(),
										 $request->getActionName());
			if(!$isAllowed) {
				$request->setActionName($loginAction)
						->setControllerName($loginController);
			}
		}
		else {
			if($request->getControllerName() == $loginController && $request->getActionName() == $loginAction)
				$request->setControllerName('index')
						->setActionName('index');
		}
	}
}
